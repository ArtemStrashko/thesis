\newpage
\chapter{Organic polariton lasing}
\label{org_pol_lasing_chapter}

Dye lasers are known to feature low-threshold lasing. In contrast to simple two-level systems, where electronic state inversion is necessary for lasing, 
the existence of ro-vibrational levels in molecules allows an inversionless lasing. Two basic components needed for lasing are an optically active inverted 
transition and an optical cavity to collect photons into a given mode. If we consider only ground and the first excited electronic state, and on top of this 
a single vibrational molecular mode, we can enumerate all the states by a the following pair $(\uparrow/\downarrow, n)$, where $\uparrow/\downarrow$ represents 
an excited or a ground electronic state and $n$ - the number of phonons in this state. While in a two-level system we always have $n=0$ and so lasing may 
be driven by $(\uparrow, 0) \rightarrow (\downarrow, 0)$ transition only (where $(\uparrow, 0)$ is incoherntly populated via excitation and subsequent 
relaxation of higher-energy states), in a molecule $(\uparrow, 0) \rightarrow (\downarrow, 1)$ is usually responsible for 
lasing (hereafter $ (\downarrow, n) \rightarrow (\uparrow, m)$ is replaced by just $(n - m)$), thus requiring inversion between $(\uparrow, 0)$ and $(\downarrow, 1)$ states, which is easy to reach at temperatures lower than corresponding frequency of a molecular transition $\omega_v$ because at $k_B T \ll \hbar \omega_v$ 
the state $(\downarrow, 1)$ is almost empty. Therefore, tuning cavity frequency to a $(1 - 0)$ transition, it is possible 
to reach a lower-threshold lasing with respect to a two-level system due to softening of the electronic state inversion requirement \cite{dye_lasers}. 

In the very same system polaritonic physics can explored. Indeed, increasing the number of molecules and/or improving a cavity quality factor, a 
regime of strong matter coupling is routinely achieved in experiments. Increasing external pumping of such a system, polariton lasing can be 
reached. It is widely believed that polariton lasing is low-threshold because its mechanism is based on stimulated scattering from the reservoir 
to the polariton condensate, which does not require inversion, in contrast to an ordinary laser, which does require inversion. This is considered 
to be one of the main advantages of a polariton laser (from the results of this chapter it follows that this argument is not quite right). 

However, until recently, there has been no systematic investigation of how strong matter-light coupling modifies the physics of a dye laser, so these 
two inherently connected systems have been considered apart from each other. Moreover, the effect of polariton formation on lasing threshold has never been 
studied, although speculated frequently. 

Therefore, the aim of this chapter is to describe the transition from a well-known regime of a molecular laser (weak matter-light coupling) to the regime of 
polariton laser (strong coupling), to understand similarities and differences, to propose a unified framework for the description of these systems, and to find 
optimal parameters for low-threshold lasing. To address these points, a microscopic model will be introduced in Section~\ref{OPL_model}. 
Section~\ref{OPL_solution} describes an exact (in the thermodynamic limit) solution of this model. In Section~\ref{OPL_results} the results are discussed. 


\section{Model}
\label{OPL_model}

To describe lasing we need a theory of a driven-dissipative system. A widely used approach is to separate the full system into a ``system'', 
the physics of which we want to reveal, and a ``bath'', the effect of which on the ``system'' we want to understand, and which is not  
affected by the system. So, let us consider a Hamiltonian $H = H_S + H_B + H_{int}$, where $H_S$ represents molecules in a microcavity coupled to its 
photon mode (system), $H_B$ is responsible for incoherent driving, dissipation and dephasing of the system, and $H_{int}$ is the system-bath interaction. 

\subsection{System Hamiltonian}

Let us consider the system Hamiltonian first. Although molecules usually feature a huge number of transitions, to make calculations possible, we need to have 
some simplified model. The main ingredients we need to describe a molecular polariton laser are an electronic transition with finite optical matrix element and a molecular vibrational mode. Thus, we have 
\begin{equation}
\label{opl_Hamilt}
\begin{split}
H_S =
  \sum_n
  \left\{
    \frac 1 2 \varepsilon \sigma_n^z  + 
  \omega_v
  \left[
    b^{\dagger}_n b_n^{\mathstrut} +
    \sqrt{S} \sigma_n^z (b^{\dagger}_n + b_n^{\mathstrut})
  \right]
  \right\}  \\  + 
    \omega_c a^{\dagger} a   + 
    g \left( a^{\dagger} + a \right) \sum_n \sigma_n^x + 
    \frac{g^2 \Nmol}{\varepsilon} (a^{\dagger} + a)^2,
\end{split}
\end{equation}
where there is a sum over all molecules $n$, $\sigma^{x,y,z}$ are Pauli matrices, $\varepsilon$ is the energy of an electronic transition, 
$\omega_v$ is the frequency of a vibrational mode, $b_n$ is an annihilation operator of a molecular phonon (a quanta of local/internal molecular vibrational 
mode), $S$ characterizes the coupling strength of the electronic and molecular transitions, $\Nmol$ is the total number of molecules, $g$ is the matter-light 
coupling strength, $a$ is a photon annihilation operator. The last term is a so-called $A^2$-term, which must be kept to avoid unphysical superradiant 
transition in the strong-coupling regime as it renormalises the lower polariton mode. The Hamiltonian (\ref{opl_Hamilt}) contains only a single photon mode 
with energy $\omega_c$.
This serves as a simplification to understand the basics  of a very complex model of a molecular polariton laser and to provide a foundation for further 
calculations with more realistic models containing multiple photon modes. 



\subsection{Incoherent processes}

There are a number of incoherent processes that needed be taken into account to model lasing of a system of molecules coupled to a microcavity photon. 
The most obvious one is the leakage of photons out of a cavity due to finite transmission of cavity mirrors. This may be described by introducing a 
Hamiltonian of free-space --- non-cavity --- photons $H_{nc} = \sum_{\vec{k},n} \hbar \omega_k \tilde{a}^{\dagger}_{\vec{k},n} \tilde{a}^{\mathstrut}_{\vec{k},n}$ 
and coupling them to a cavity photon 
$H_{leak} = \sum_{\vec{k},n} \xi_{\vec{k},n} \left( a\tilde{a}^{\dagger}_{\vec{k},n} + a^{\dagger} \tilde{a}^{\mathstrut}_{\vec{k},n} \right)$. 
Supposing that the density of non-cavity modes is constant in the range of cavity photon frequencies (which differ from $\omega_c$ in the strong matter-light 
coupling regime), that the state of non-cavity photons is not affected by a cavity photon, and that in-out cavity photon coupling may be treated perturbatively, 
then the dynamics of a cavity photon may be described by the Lindblad master equation:

\begin{equation}
\partial_t \rho_S = - i \left[ H, \rho_S \right] + \kappa \mathcal{L}[a]
\end{equation}
(neglecting small Lamb shift of system frequencies) where $\rho_S$ is a system density matrix (a microcavity photon here), $H = \omega_c a^{\dagger} a$ 
--- system Hamiltonian, $\kappa$ --- photon leakage rate and $\mathcal{L}[X] = X \rho X^{\dagger} - \frac 1 2 \left[ X^{\dagger} X, \rho \right]_{+}$ --- 
Lindblad operator, describing photon leakage. However, in the strong coupling regime the assumption of constant density of non-cavity modes is not well 
justified. While removing this assumption requires numerically complicated non-Markovian treatment of an already complex system, its effect is not 
expected to change the results considerably. Indeed, while increased frequency of the upper polariton may lead to enhanced leakage, reduced lower 
polariton frequency competes with this effect due to corresponding lowered leakage. 

Supposing that all incoherent processes satisfy the assumptions of the Lindblad master equation derivation (system-bath coupling is small in comparison 
with inter-system interactions, bath is not affected by the system), we can write the most general master equation 
describing evolution of the system (molecules and a microcavity photon) density matrix:

\begin{equation}
\label{lindblad_init}
\partial_t \rho(t) = - i \left[ H, \rho \right] + \sum_n \gamma_i \mathcal{L}[O^i_n]
\end{equation}
where $\gamma_i$ are rates of incoherent processes, $O^i_n$ --- corresponding jump operators and $\sum_n$ is the sum over all molecules. The cartoon 
Fig.~\ref{weak_coupling_summary}(a) illustrates the system and all incoherent processes considered. 
\begin{figure}[h!]
\center{\includegraphics[width=5in]{figures/weak_coupling_summary}}
\caption{ (a) Cartoon illustrating the model: many molecules (N-level systems) are coupled to a cavity mode. 
        (b)--(d) Weak coupling behavior. (b) Emission and absorption spectra of the molecules. (c) Weak coupling 
        phase diagram. The cyan dashed line marks the phase boundary without vibrational dressing ($S = 0$). 
        (d) Dominant molecular transitions coupled into the lasing mode at threshold. 
        Parameters used: $\varepsilon \equiv 1.0$, $S = 0.1$, $\omega_v = 0.2$, $\Gamma_{\downarrow} = \kappa = 10^{-4}$, 
            $\Gamma_z = 0.03$, $\gamma_v = 0.02$, $k_B T_v = 0.025$, $N_v = 4$}
\label{weak_coupling_summary}
\end{figure}


First, as discussed above, photon leakage is modelled by the term $\kappa \mathcal{L}[a]$. Next, incoherent excitation of an electronic subsystem 
is described by the term $\Gamma_{\uparrow} \mathcal{L}[\sigma^{+}]$. Incoherent decay of an electronic excitation into non-cavity modes 
(which is responsible for a less than $100\%$ quantum yield) is modelled by the term $\Gamma_{\downarrow} \mathcal{L}[\sigma^{-}]$. 
Interaction of an electronic subsystem with bath modes, which do not lead to real electronic transitions, is modeled phenomenologically 
by including a term $\Gamma_z \mathcal{L}[\sigma^z]$ describing dephasing. Scattering of intramolecular phonons (vibrons) with low-energy 
bath modes leads to thermalisation of molecular oscillations, which is described by the sum of terms 
$\gamma_{\uparrow} \mathcal{L}[b^{\dagger} - \sqrt{S} \sigma^z]$ and $\gamma_{\downarrow} \mathcal{L}[b - \sqrt{S} \sigma^z]$ 
accounting for the electronic-state-dependent vibrational displacement with $\gamma_{\uparrow} = \gamma_{v} n_B$ and 
$\gamma_{\downarrow} = \gamma_{v} \left( n_B + 1 \right)$, $n_B = \left[ \exp(\omega_v / k_B T) - 1 \right]^{-1}$. 
From the form of these terms, it is clear that the the first one describes absorption of a bath phonon by a molecule, 
while the second one --- stimulated emission. 



\subsection{Absorption and emission spectrum}

In the previous section, a rather large number of parameters describing incoherent rates was introduced, which warns that such a theory may describe anything 
upon tuning these parameters. However, the way to choose these parameters is to have physically motivated absorption and emission molecular spectra, which 
strongly narrows the region of possible parameter regimes. Moreover, as shown in \cite{polariton_lasing_PRL}, the main conclusions of this theory are robust 
with respect to fine tuning of parameters. Below we will be using $\varepsilon \equiv 1.0$ by the definition 
(so measuring energies in units of the electronic transition energy; while typical physical values are $\varepsilon \simeq 1-2$ eV), 
numerical values of other parameters are given in a caption of the Fig.~\ref{weak_coupling_summary}. 
With these parameters using the Quantum Regression theorem \cite{Open_Q_Systems_Breuer_Pettruccione}, one can get emission and absorption spectrum 
of a molecule (essentially in the weak molecule-light coupling limit) --- Fig.~\ref{weak_coupling_summary}(b). 
These spectra make perfect sense. Indeed, there is a zero-phonon line at $\omega \simeq \varepsilon$ and absorption (emission) mirror-symmetric 
shoulders with clear one and two phonon peaks. 








\section{Method}
\label{OPL_solution}

The goal is to obtain a non-equilibrium phase diagram of a system described above in the thermodynamic limit taking into account both strong matter-light 
coupling (hereafter --- exciton-photon coupling) and strong coupling of an electronic transition to intramolecular vibration 
(hereafter --- exciton-phonon coupling). 

If exciton-phonon coupling is weak, one can integrate intramolecular phonons out and get just an extra term in the Lindblad equation for a 
two-level system coupled to a photon. For weak coupling to light, one can make a polaron transformation and then treat matter-light coupling perturbatively, 
which leads to rate equations \cite{phot_condensation_PRL, phot_condensation_PRA}, describing photon condensation. 
For a small number of molecules, exact numerical methods are available, which treat the vibrational modes as a non-Markovian dissipation process
\cite{Tensor_network_polariton_PRB, Tensor_network_polariton_PRL}. However, the thermodynamic limit with many molecules and both strong matter-light and 
exciton-phonon coupling has not been addressed before, so the aim of this section is to solve this problem \cite{polariton_lasing_PRL}. 

The solution is based on two crucial ideas. The first idea, which is illustrated in the Fig.~\ref{GG_matrices}, is to switch from a two-level system plus a 
phonon description to a new ``molecular'' basis, so to describe a molecule as an $N$-level system, rather than as a combination of an electronic and vibrational subsystems. Here $N = 2 N_v$, where $N_v$ is a number of molecular vibrational levels taken into account (i.e. states with zero, one, ..., $(N_v - 1)$ phonons). 
This allows to avoid any exciton-phonon decoupling and so to treat exciton-phonon coupling exactly. As shown in an appendix of \cite{polariton_lasing_PRL}, 
in practical calculations, for $S = 0.1$ inclusion of as few as $N_v = 4$ vibrational levels already leads to convergent results. 
\begin{figure}[h!]
\center{\includegraphics[width=4in]{figures/GG_matrices}}
\caption{From 2-level system plus a phonon to an $N$-level system}
\label{GG_matrices}
\end{figure}

For the description of a general N-level system, one can use a basis of Generalised Gell-Mann matrices $\lambda_i$ \cite{book_GGM}, satisfying 
$\Tr \left( \lambda_i \lambda_j \right) = 2 \delta_{ij}$ (which generalize a set of Pauli matrices serving a basis of Hermitian traceless two-by-two matrices). 
This allows to rewrite any operator (represented as N-by-N matrix) as follows $O = \frac 1 2 \lambda_i \Tr \left( O \lambda_i \right)$ 
(with $\lambda_0 = \sqrt{(2/N)} \vec{1}_N$). Therefore, the Hamiltonian (\ref{opl_Hamilt}) can be rewritten in terms of new operators:
\begin{equation}
H = \omega_c a^{\dagger} a + \sum_n \left[ A_i + B_i (a^{\dagger} + a) \right] \lambda_i^{(n)} + \frac{g^2 \Nmol}{\varepsilon} (a^{\dagger} + a)^2
\end{equation}
and the Lindblad equation (\ref{lindblad_init}) as well:
\begin{equation}
\label{lindblad_GGM}
\partial_t \rho(t) = - i \left[ H, \rho \right] + \kappa \mathcal{L}[a] + \sum_{\mu n} \mathcal{L} \left[ c_i^{\mu} \lambda_i^{(n)} \right]
\end{equation}
where $n$ is a number of a molecule, $\mu$ is the number of one of the five molecular dissipative channels in Eq.~(\ref{lindblad_init}), summarized in 
Fig.~\ref{weak_coupling_summary}. Rewriting dissipative terms in Eq.~(\ref{lindblad_init}) as $\sum_{\mu} \Gamma_{\mu} \mathcal{L}[J_{\mu}]$, 
the coefficients in Eq.~(\ref{lindblad_GGM}) are $c_i^{\mu} = \sqrt{\Gamma_{\mu}} \Tr \left( J_{\mu} \lambda_i \right) / 2$. 









Having introduced a tool for non-perturbative description of exciton-phonon coupling, we now need to find a way to treat strong exciton-photon 
coupling. This leads to the second crucial idea of employing a mean-field matter-light decoupling, which was shown to be exact in the thermodynamic 
limit \cite{superrad_PRL}. This can be done simply by deriving equations of motion for variables $\alpha = \langle a \rangle$ and 
$\ell_i = \langle \lambda_i \rangle$, where $\langle O \rangle = \Tr(\rho O)$, and then assuming 
$\langle a \lambda_i \rangle = \langle a \rangle \langle \lambda_i \rangle$ (mean-field decoupling). 
This leads to a set of coupled differential equations:
\begin{align}
\label{OPL_equations_main}
\partial_t \alpha = 
        - \left( i \omega_c + \frac{\kappa}{2} \right) \alpha 
        - 4 i \frac{g^2 \Nmol}{\varepsilon} Re[\alpha] - 
        i \Nmol B_i \ell_i
\\ 
\partial_t \ell_i = 
        \left( \xi_{ik} + 4 f_{ijk} B_j Re[\alpha] \right) \ell_k + \frac{4i}{N} c_j^{\mu} c_k^{\mu *} f_{ijk}
\end{align}
where $\xi_{ik} = 2 f_{ijk} A_j + i c_{l}^{\mu} c_m^{\mu *} (f_{ilp} \zeta_{pmk} + f_{mip} \zeta_{plk})$ with 
$\zeta_{ijk} \equiv \Tr (\lambda_i \lambda_j \lambda_k)/2$, and $f_{ijk} = \Tr([\lambda_i, \lambda_j] \lambda_k) / 4i$. 



Mean-field theory shows a phase transition between a normal state with $\alpha = 0$, and a symmetry-broken state with $\alpha \neq 0$ which is 
denoted as a laser (but which may be either a photon or polariton laser depending on the interplay of coherent and incoherent processes). 
One can time evolve the equations for $\alpha$ and $\ell_i$ (starting from some small but finite $\alpha$) or even simpler --- study linear 
stability of equations of motion. Writing the variables as combinations of normal state solution and fluctuations $\alpha = \delta \alpha$, 
$\ell_i = \ell_{i,ns} + \delta \ell_i$, where $\ell_{k,ns} = - \frac{4i}{N} c_j^{\mu} c_k^{\mu*} f_{ijk} \xi_{ik}^{-1}$ 
(with $\xi_{ik}^{} \xi_{ik}^{-1} = \delta_{ik}$), and linearising equations, one can obtain equations for fluctuations 
$\partial_t \upsilon = \mathcal{M} \upsilon$, where $\upsilon = (\delta \alpha, \delta \alpha^{*}, \delta \ell)^{\intercal}$, and then find 
the eigenmodes $\mathcal{M} \upsilon^k = \xi^k \upsilon^k$. Analysing eigenvalues $\xi^k$ one can find regimes where the systems is stable or 
unstable (fluctuations proliferate or decay) and so deduce a system phase diagram. Indeed, $Re[\xi^k]$ gives the growth (positive) or decay
(negative) rate of a mode, while $Im[\xi^k]$ gives its oscillation frequency. From the eigenvectors $\upsilon^k$ one can also find the contributions 
of different molecular transitions to the given unstable mode (for the details see Appendix~\ref{molecular_transition_from_dens_matr}). 



\section{Results}
\label{OPL_results}

\subsection{Weak coupling}

Before attempting to understand the effect of strong matter light coupling, it is instructive to consider a weak coupling limit first. 
The strength of matter-light coupling is usually characterized by a ratio of a ``bare'' Rabi frequency $g \sqrt{\Nmol}$ (which is the 
splitting between the lower and upper polariton in the absence of vibrational dressing $S=0$) and electronic transition energy $\varepsilon$. 
So, a weak coupling limit corresponds to $g \sqrt{\Nmol} / \varepsilon \ll 1$. 

Fig.~\ref{weak_coupling_summary} shows a summary of weak coupling results at $g \sqrt{\Nmol} = 0.05$ (so it basically describes how an ordinary 
molecular laser operates) and a system cartoon. 
The weak coupling phase diagram, Fig.~\ref{weak_coupling_summary}(c), is straightforward to understand. The cyan dashed line shows the lasing-normal 
phase boundary for 2-level molecules (without exciton-phonon coupling $S=0$), while black and white regions represent lasing and normal phases 
when considering the full model. The lasing threshold at $S \neq 0$ is reduced with respect to $S=0$ case in the region $\omega_c < \varepsilon$, 
where emission is stronger than absorption, and increased in an opposite region, which makes perfect sense. Indeed, for lasing with a two-level 
system one needs inversion, which requires pumping to be stronger than decay $\Gamma_{\uparrow} > \Gamma_{\downarrow}$. However, as discussed in 
the beginning of this section, in the case of vibrational dressing, a different transition may be employed for lasing, e.g. 
$(1-0)$ which is already inverted at much lower pumping $\Gamma_{\uparrow}$ because of exponentially small 
population of the state $(\downarrow, 1)$ at $k_B T_v \ll \omega_v$. Therefore the peaks of reduced lasing threshold at $\omega_c < \varepsilon_0$ 
originate from transitions to final states with non-zero number of phonons. The anti-peaks at $\omega_c > \varepsilon$ correspond to maxima of 
absorption, where it is much stronger than emission. At $\omega_c = \varepsilon_0$ pumping threshold with vibrational dressing is the same as without 
it, i.e. $\Gamma_{\uparrow}^{th} = \Gamma_{\downarrow}$. 



Next, Fig.~\ref{weak_coupling_summary}(d) shows the composition of the unstable mode at the lasing threshold, i.e., along the boundary 
between lasing and normal states shown in Fig.~\ref{weak_coupling_summary}(c). In the region where the threshold is low 
(around $\omega_c = \varepsilon - \omega_v$), the $(1 - 0)$ transition contributes most, while where the threshold is high 
(around $\omega_c = \varepsilon + \omega_v$), the $(0 - 1)$ transition dominates. Other transitions are not shown as they weight is generally 
much lower. 








\subsection{Strong coupling}

Having understood the physics in the weak coupling limit, we can now explore how strong exciton-photon coupling modifies the phase diagram, 
and understand the physics behind. Fig.~\ref{pump_omc_ph_diagr__lm_coupling_goes_up} shows the evolution of a $(\omega_c, \Gamma_{\uparrow})$ 
phase diagram as exciton-photon coupling $g \sqrt{\Nmol}$ goes up, in the low pumping regime to see whether strong coupling is a 
direct route to low threshold lasing. As the system enters the strong coupling regime $g \sqrt{\Nmol} = 0.1$ (roughly speaking when $g \sqrt{\Nmol}$ 
exceeds molecular transition linewidth, which is mostly controlled by dephasing $\Gamma_z$ in our model), the form of a phase diagram basically 
remains the same as in the weak coupling limit. However, as $g \sqrt{\Nmol}$ increases, the most striking feature is that the lobe at 
$\omega_c = \varepsilon - \omega_v$ (due to $(1-0)$ transition) bends and, at weak pumping around 
$\Gamma_{\uparrow} \simeq 0.5 \Gamma_{\downarrow}$, extends to significantly higher cavity frequencies, which finally leads to a re-entrant
behaviour. 

\begin{figure}[ht]  
\includegraphics[width=0.99\linewidth]{figures/pump_omc_ph_diagr__lm_coupling_goes_up}
\caption{Evolution of phase diagrams with increasing coupling, $g \sqrt{\Nmol}$ (values as shown).  
  Dash-dotted (yellow) lines indicate the cuts shown in
  Fig.~\ref{spectrum_and_DM_cuts__01},\ref{spectrum_and_DM_cuts__10}.  Parameters as in Fig.~\ref{cartoon}.}
\label{pump_omc_ph_diagr__lm_coupling_goes_up}
\end{figure}



To understand the mechanism responsible for the extension of low threshold lasing region to higher cavity frequency, we need to explore the 
nature of unstable modes. Again, to make a firm foundation for strong coupling analysis, it is instructive to start from a more straightforward 
case of moderate coupling $g \sqrt{\Nmol} = 0.1$. Figure~\ref{spectrum_and_DM_cuts__01} shows the composition of the unstable mode (lower panels), 
frequencies of all system eigenmodes (upper panels), and gain of the unstable mode along three fixed pumping cuts across the top left phase diagram 
Fig.~\ref{pump_omc_ph_diagr__lm_coupling_goes_up}. In Fig.~\ref{spectrum_and_DM_cuts__01}(a), at low pumping $\Gamma_{\uparrow} = 0.4 \Gamma_{\downarrow}$, 
we can see polaritonic splitting where photon\footnote{here we use effective photon frequency, which comes from the diagonalisation of a photonic part 
of the Hamiltonian (\ref{opl_Hamilt}), which gives effective frequency ${ \omega_c^{\text{eff}} = \sqrt{\omega_c (\omega_c + 4 g^2 \Nmol / \varepsilon)} }$ 
and effective coupling ${ g^{\text{eff}} = g \sqrt{\omega_c / \omega_c^{eff}} }.$}  
resonates with the excitonic $(0 - 0)$ transition (which does not involves phonons), i.e. where 
$\omega_c = \varepsilon$. There is one more anti-crossing point at higher photon frequency due to a transition involving a phonon.
However, corresponding anti-crossing is much smaller because this molecular transition is much more weakly coupled to a photon\footnote{in the second order perturbation 
theory for a transition involving $n$ phonons in the electronically excited state, it is $g^2 S^n e^{-S} / n!$}, and emission from this state is very weak due to 
fast relaxation to the lowest energy state in the excited electronic state manifold. There is no anti-crossing at smaller photon frequency because 
ground electronic state with finite number of phonons is not activated at low temperatures, so there is no absorption from this state. As pumping 
increases --- panels (b,c) --- polaritonic splitting goes away due to extra effective dephasing caused by stronger pumping. 
At weak pumping, $\Gamma_{\uparrow} = 0.4 \Gamma_{\downarrow}$, lasing develops when photon frequency $\omega_c$ crosses the $(1 - 0)$ or $(2 - 0)$ transitions, 
as expected. Indeed, as can be seen from the Fig.~\ref{spectrum_and_DM_cuts__01}(d), the unstable mode mostly involves $(1-0)$ transition, evolving to 
$(0 - 0)$ transition as $\omega_c$ approaches the zero-phonon line. As pumping increases, lasing becomes possible over a wider range of photon frequencies. 
When excitonic pumping $\Gamma_{\uparrow}$ exceeds decay rate $\Gamma_{\downarrow}$, electronic transition becomes inverted, so lasing becomes possible 
at $\omega_c = \varepsilon$. As pump power increases further, lasing can be achieved even in the region of $\omega_c$ where absorption is stronger than
emission. However, as can be seen in the Fig.~\ref{spectrum_and_DM_cuts__01}(c), high-frequency lasing develops in the region of the smallest absorption-emission 
ratio first. 
\begin{figure}[t]  
\includegraphics[width=0.99\linewidth]{figures/spectrum_and_DM_cuts__01}
\caption{Nature of lasing instability at $g \sqrt{\Nmol} = 0.1$.  Top (a--c): Real (right, solid cyan) and imaginary (left, grayscale) parts of linearized 
        eigenvalues $\xi$.  The grayscale indicates the photon component of that mode.  The yellow dashed line   highlights which imaginary part corresponds 
        to the mode that is unstable (has a positive real part). The pink dotted line shows the effective photon frequency, $\omega_c^{\text{eff}}$.  
        Bottom (d--f): Vibrational composition of unstable mode. Parameters as in  Fig.~\ref{cartoon}. }
\label{spectrum_and_DM_cuts__01}
\end{figure}
At even stronger pumping, low- and high-frequency lasing regions finally merge. This can be clearly seen in the full $g \sqrt{\Nmol} = 0.1$ 
phase diagram in the Fig.~\ref{full_coupling_01_ph_diagr}, which clearly shows low-pumping peaks due to local maxima of emission, and high-pumping anti-peaks 
due to absorption maxima. 



\begin{figure}
\includegraphics[width=0.9\linewidth]{figures/full_coupling_01_ph_diagr}
\caption{Full $g \sqrt{\Nmol} = 0.1$ phase diagram}
\label{full_coupling_01_ph_diagr}
\end{figure}







At stronger coupling, $g \sqrt{\Nmol}$, the physics changes considerably. First, in the low-pumping regime $\Gamma_{\uparrow} = 0.4 \Gamma_{\downarrow}$, 
Fig.~\ref{spectrum_and_DM_cuts__10}(a), we have a very strong anticrossing, which now involves many transitions, in contrast to mostly $(0-0)$ for 
$g \sqrt{\Nmol} = 0.1$. In addition, Fig.~\ref{spectrum_and_DM_cuts__10}(a) shows a a new feature: a self-tuning effect, where the unstable polaritonic 
mode locks with a $(1-0)$ vibrational sideband driving lasing. Therefore, in spite of a strong exciton-photon detuning, frequency locking allows lasing 
at a wide range of photon frequencies in the strong-coupling regime because of self-tuning of the lower polariton to allow feeding by the $(1-0)$ molecular 
transition. 
\begin{figure}[t]  
\includegraphics[width=0.99\linewidth]{figures/spectrum_and_DM_cuts__10}
% \caption{Nature of lasing instability at $g \sqrt{\Nmol} = 1.0$}
\label{spectrum_and_DM_cuts__10}
\end{figure}

As pumping goes up, Fig.~\ref{spectrum_and_DM_cuts__10}(b), frequency locking reduces due to effectively reduced matter-light coupling by extra 
dephasing caused by stronger pumping. Another notable feature is that now the unstable mode frequency pins to the $(0-0)$ transition. As pumping increases 
further, Fig.~\ref{spectrum_and_DM_cuts__10}(c), polaritonic splitting gets smaller. At the same time, in contrast to previous cases, we get frequency pinning 
with the $(0-1)$ transition, which is also clear from the Fig.~\ref{spectrum_and_DM_cuts__10}(f). Moreover, as frequency pinning goes away at high enough bare 
photon frequency, the system still supports lasing, but at a bare photon frequency --- Fig.~\ref{polariton_and_photon_lasing}. Therefore, in the strong 
coupling and pumping regime the system demonstrates a transition from polariton to photon lasing as a detuning does up. 

\begin{figure}[t]  
\includegraphics[width=0.9\linewidth]{figures/polariton_and_photon_lasing}
\caption{Transition from polariton to photon lasing}
\label{polariton_and_photon_lasing}
\end{figure}


To check the effect of stronger dephasing on a phase diagram, in Fig.~\ref{incoherent_effects} phase diagrams with increased coupling 
of vibrational mode to bath phonons $\gamma_v$ and with increased electronic dephasing rate are demonstrated. Clearly, increased dephasing 
rate $\Gamma_z$ has the strongest effect on a phase diagram reducing the effect of strong matter-light coupling. This makes sense because 
enhancing $\Gamma_z$ increases affects excitonic linewidth, which reduces effective exciton-photon coupling. 

\begin{figure}[t]  
\includegraphics[width=0.9\linewidth]{figures/incoherent_effects}
\caption{Dependence of phase diagram at $g \sqrt{\Nmol} = 1.0$  on varying vibrational damping $\gamma_{v}$ and excitonic dephasing rate $\Gamma_z$}
\label{incoherent_effects}
\end{figure}



\subsection{Pumping threshold versus matter-light coupling strength \& optimal cavity frequency $\omega_c$}

Having understood the effect of strong matter-light coupling on a lasing phase diagram, we can now address the questions of whether strong coupling 
is a direct route to low-threshold lasing, in other words whether polariton laser outperforms an ordinary molecular one in terms of lasing threshold. 
Fig.~\ref{lasing_threshold_phase_diagrams}(a-c) show $(\Gamma_{\uparrow}, g \sqrt{\Nmol})$ phase diagrams when photon resonates with $(1-0)$, 
$(0-0)$ and $(0-1)$ transitions respectively. In the regime of a ``good molecular laser'', where $\omega_c = \varepsilon - \omega_v$ 
($\omega_c = 0.8$ in our case), strong-coupling phase boundary matches weak-coupling results (red dashed line) up to a unimportant normal region where 
coupling is strong and pumping is relatively high. This suggests that strong coupling does not improve a frequency-optimised molecular laser. 
If we go beyond optimal photon frequency and tune $\omega_c$ to $(0-0)$ or $(0-1)$ transition, Fig.~\ref{lasing_threshold_phase_diagrams}(b,c), 
where $\Gamma_{\uparrow} > \Gamma_{\downarrow}$ is necessary (but not sufficient) for lasing in the weak-coupling theory, strong coupling does 
lead to reduced threshold. Therefore, strongly-coupled polaritonic lasing outperforms a weakly-coupled molecular laser if cavity frequency is 
not optimised, i.e. when $\omega_c \neq \varepsilon - \omega_v$. 
\begin{figure}[t]  
\includegraphics[width=0.9\linewidth]{figures/lasing_threshold_phase_diagrams}
\caption{(a)--(c) Phase diagrams at different cavity frequencies. The red dashed line shows the weak-coupling theory phase boundary \cite{polariton_lasing_PRL}. 
         Panel (d) shows how minimal critical pumping strength (optimized over cavity frequency) depends on matter-light coupling. The red shading highlights 
         the crossover from the weak- to strong-coupling region (where light-matter coupling exceeds rates of incoherent processes)}
\label{lasing_threshold_phase_diagrams}
\end{figure}

To see how lasing threshold pumping depends on the strength of matter-light coupling, in Fig.~\ref{lasing_threshold_phase_diagrams}(d) I plot 
threshold $\Gamma_{\uparrow} / \Gamma_{\downarrow}$ optimised over a bare cavity frequency $\omega_c$ versus bare Rabi splitting $g \sqrt{\Nmol}$. 
These results suggest that stronger coupling does lead to lower-threshold lasing. However, as soon as the system enters the strong-coupling regime, 
not much threshold reduction can be achieved (only a few per cent) upon increasing coupling further if $\omega_c$ is optimised. 





While Fig.~\ref{lasing_threshold_phase_diagrams}(d) shows how lasing threshold depends on coupling at optimised frequency, it doesn't show how to choose 
optimal frequency, which is an essential experimental parameter. So, Fig.~\ref{optimal_frequency} shows bare photon frequencies at which lasing threshold 
has local minima versus matter-light coupling. Firstly, there is no lasing below critical coupling strength, which is around $g \sqrt{\Nmol} \approx 0.0024$ 
in our model. As coupling becomes strong enough to allow lasing, due to very inefficient coupling to $(n-0)$ transitions, the lasing is driven $(0-0)$ 
transition. As coupling increase further, another minimum due to $(1-0)$ transition arises, which soon becomes a global minimum (standard molecular 
laser case). At even stronger coupling, local minima due to higher-order transitions $(2-0)$ and $(3-0)$ (which can be also seen in  
Fig.~\ref{pump_omc_ph_diagr__lm_coupling_goes_up}) arise. 
\begin{figure}
\includegraphics[width=0.9\linewidth]{figures/optimal_frequency}
\caption{Optimal photon frequency vs matter-light coupling. Shaded region highlights global minimum. }
\label{optimal_frequency}
\end{figure}

Increasing coupling further into the strong-coupling regime leads to quite non-trivial evolution of the positions of local minima. The behaviour of the 
$(1-0)$ branch can be associated with the self-tuning at strong coupling, which allows lasing threshold for this transition to move to higher photon 
frequency. In contrast, the $(2-0)$ and $(3-0)$ transitions do not show any self-tuning effect because they are not strongly coupled to a photon, which may 
be seen (although not rigorously) from the lowest-order perturbation theory, which gives corresponding emission (absorption) matrix element 
$g^2 S^n e^{-S} / n!$. At the same time, upon increasing matter-light coupling, effective photon frequency goes up 
${ \omega_c^{\text{eff}} = \sqrt{\omega_c (\omega_c + 4 g^2 \Nmol / \varepsilon)} }$. Therefore, for a photon to resonate with $(2-0)$ or $(3-0)$ 
transitions, its bare frequency should be reduced in the strong-coupling region to cope with effective photon frequency growth. 









To sum up, although strong matter-light coupling does not reduce molecular laser pumping threshold, it does increase the range of photon frequencies at which 
low-threshold lasing can be achieved, as Fig.~\ref{pump_omc_ph_diagr__lm_coupling_goes_up} clearly demonstrates. 



















