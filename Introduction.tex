\newpage
\chapter{Introduction}

Nature generously supplies us with new materials which demonstrate unusual properties, set up new challenges, widen our knowledge horizons 
and sometimes lead to technological advances. However, the search for material with desired properties is not a trivial task, which requires 
extensive theoretical calculations, a lot of experimental data and statistical analysis (for example \cite{stanev2018machine}) to guide the hunt. 
A recent triumph of finding a material with unprecedented properties was the discovery of a topological insulator \cite{konig2007quantum}, which was 
preceded by theoretical calculations \cite{bernevig2006quantum}. 

A different approach to obtain new material properties is to engineer them by means of external handles. For example, lowering temperature led 
to the discovery of superconductivity \cite{onnes1911commun}, application of high pressure --- to almost room $T_c$ superconductivity 
\cite{PhysRevLett.122.027001}, application of strong magnetic field to a two-dimensional electron gas --- to the Quantum Hall Effect 
\cite{iQHE, fQHE}. More recent examples include Floquet Engineering \cite{Floquet_Engineering} of material properties and driving by strong 
laser pulses, leading, for example, to transient superconducting-like states \cite{K3C60_supercond_first, K3C60_supercond_second}. 
However, the last two approaches suffer an ubiquitous problem of heating, which seems to be the main problem for practical long time-scale 
applications. In contrast, strong coupling of material excitations to quantum light does not have such an issue. 

Recent experimental advances in fabrication of optical microcavities opened up an avenue to study how strong coupling of light to excitations 
of matter can affect various material properties \cite{10, Deng_polariton_review}. A list of already experimentally demonstrated 
effects includes polariton-assisted chemistry \cite{hutchison2012modifying, wang2014phase, feist2017polaritonic}, modified magneto-transport 
of two-dimensional electron gas in magnetic field \cite{paravicini2019magneto, bartolo2018vacuum}, enhanced conductivity of an organic 
semiconductor \cite{Conductivity_in_organic_semiconductors}, polaritonically-enhanced F{\"o}rster energy transfer \cite{zhong2016non, zhong2017energy}, 
room-temperature polariton condensation (lasing) \cite{plumhof2014room, kena2010room} and superfluidity \cite{lerario2017room}, 
polaritonically-enhanced Raman scattering \cite{enhanced_raman}. 


It is the aim of this thesis to model some of these experimental systems and extend the list of effects driven by strong coupling of light 
and material excitation. Chapters \ref{Raman_chapter} and \ref{org_pol_lasing_chapter} concern the effects of strong-matter light coupling 
on properties of organic molecules featuring strong coupling between their intramolecular electronic and vibrational excitations (which will 
be called exciton-vibron coupling), while Chapter~\ref{imb_cond_chapter} discusses how a phase diagram of an imbalanced electron-hole 
system is affected by strong coupling of an excitonic transition to a photon. 

In particular, Chapter~\ref{Raman_chapter} discusses how strong matter-light coupling between an infrared cavity photon and intramolecular 
vibrons affects Raman scattering with organic molecules \cite{Raman_our_paper}, which is motivated by the experiment \cite{enhanced_raman}. 
In contrast to the experimental results, which showed three orders of magnitude enhanced Raman scattering under stong matter-light coupling, 
the results of theoretical modeling show only a moderate enhancement of Raman scattering in the ultra-strong matter-light coupling regime 
matching the outcomes of other theoretical calculations \cite{Raman_Madrid}. 

Next, Chapter~\ref{org_pol_lasing_chapter} considers a similar system of organic molecules in a microcavity and discusses the effects of 
strong coupling between a microcavity photon and electronic excitations of molecules, which also feature strong exciton-vibron 
coupling. This chapter is motivated by experiments on exciton-polariton lasing (condensation) with organic molecules, e.g. 
\cite{plumhof2014room, kena2010room}, and aims to understand the microscopic mechanism of polariton lasing, how it depends on 
the various system parameters, and to uncover the connection between an ordinary dye laser operating in a weak matter-light coupling 
regime and a more exotic polaritonic laser. This chapter presents the exact (in the thermodynamic limit) non-equilibrium phase diagram 
of a system of organic molecules strongly coupled to a single photon mode. It shows a smooth transition from a usual dye
laser to a strongly-coupled polariton laser, and so for the first time explains the physics of organic polariton lasing 
\cite{polariton_lasing_PRL}. 

The final chapter, Chapter~\ref{imb_cond_chapter}, discusses a more conventional setup of semiconductor Wannier-Mott excitons coupled to microcavity 
photons. The new physics discussed in this chapter comes from considering a charged (imbalanced) semiconductor with tunable 
electron and hole density difference set up by applying bias voltage akin to a field-effect transistor (FET). This chapter  
predicts and discusses new phases emerging from the interplay of polaritonic physics and charge imbalance. It shows how the new phases 
compete and intertwine with each other upon tuning experimentally-controlled parameters such as temperature, photon cutoff frequency and 
charge density, thus enriching the physics of imbalanced correlated many-body systems due to additional photon-mediated interaction, 
which has no counterparts in more conventional setups such as (imbalanced) superconductors in a strong magnetic field. 


The remainder of the current chapter introduces these topics in more detail. Section~\ref{Raman_intro} discusses the physics of Raman scattering, 
Section~\ref{polariton_lasing_intro} concerns polariton lasing with organic molecules, and Section~\ref{imb_cond_intro} discusses condensation 
of imbalanced Fermi-systems and conventional polaritonic condensation with inorganic semiconductors. 






\section{Raman scattering}
\label{Raman_intro}

Raman scattering (RS) spectroscopy is a powerful technique to address (usually infrared) transitions, which are not optically active. 
For example, it is widely used to study low-lying ro-vibrational molecular levels \cite{banwell1994fundamentals}. It can be also used to probe 
optically forbidden single-band excitations of superconductors \cite{RS_in_superconductors}. 

The physics of Raman scattering is straightforward and can be summarized by a single formula for a transition amplitude between two states via an 
infinite number of virtual states, which follows from the second order perturbation theory \cite{39} and reads:
\begin{equation}
\label{Raman_amplitude_general}
f_{eg} = \sum_n \frac{\langle g | V | n \rangle \langle n | V' | e \rangle}{E_n - (E_g + \hbar \omega) }.
\end{equation}
In this expression $|g\rangle$ stands for the ground (initial) state, which may represent, for example, a molecule in its ground electronic 
and vibrational state and a photon before it gets absorbed, thus $E_g + \hbar \omega$ is the energy of this state. Next, $|n\rangle$ is 
an intermediate excited state (after photon absorption) with energy $E_n$, and $|e\rangle$ is the final state, which may correspond to, 
e.g. a vibrationally excited state of a molecule in the ground electronic state. In this expression $V$ is a matter-light coupling term, 
while $V' = \alpha V$ with $\alpha$ is some numerical prefactor which depends on polarisation of an emitted photon. 

Due to naturally small matter-light coupling strength and a two-photon nature of the RS process (\ref{Raman_amplitude_general}), 
in linear spectroscopy RS signal is usually rather weak, which thus required long signal collection in pioneering RS experiments. 
This led to the development of various non-linear RS enhancement techniques \cite{demtroder2013laser} such as resonant RS, where 
excitation laser wavelength is tuned close to energy of some electronic transition, or stimulated RS, where a transition from 
excited to a final state is stimulated by another laser beam, or coherent anti-Stokes RS, where two lasers are used to drive both 
transition from the ground to excited and from the excited to the final states. Another technique is to put molecules close to 
metallic surface and so to employ the effect of strong surface plasmon electric field (usually called surface-enhanced RS) 
\cite{29,30,demtroder2013laser,moskovits1985surface}. 

In many experimental works, strong coupling of an infrared (IR) microcavity photon and a molecular vibrational mode was 
demonstrated \cite{23,24,25,26,27} and then in Ref.~\cite{enhanced_raman} a fundamentally new method to enhance 
RS was discovered, which, in contrast to the techniques discussed above, is not based on resonant excitation of molecules, 
on any fine-tuned laser driving or strong (plasmonic) surface field effects. The effect demonstrated in Ref.~\cite{enhanced_raman} is claimed 
to originate from strong coupling of an infrared photon and a molecular vibron only (i.e. from formation of vibron-polariton). 
Although this requires a vibrational transition to be active both in RS and IR (and also strong enough IR absorption and 
emission to allow strong matter-light coupling), thus hardly motivating such a setup for a linear RS spectroscopy, nevertheless 
it provides an example of how single molecule properties may be tuned by collective strong coupling, i.e. by engineering the properties 
of environment (microcavity). Therefore, the nature of this effect is of fundamental interest. 

Let us now discuss the experiment of Ref.~\cite{enhanced_raman} in more detail. The experiment of Ref.~\cite{enhanced_raman} demonstrated 
that when molecules are placed into an infrared microcavity, the formation of vibron-polaritons leads to peculiar consequences in Raman 
scattering (RS). Particularly, the Raman transition signal from the scattering between the ground and the first vibron excited state splits 
between scattering to lower and upper vibron-polariton states. Also, quite surprisingly, Rabi splitting in RS signal turned out 
to be about twice larger than in IR absorption measurement. However, the most remarkable result of that experiment is the 
enhancement of the total RS cross-section by up to 3 orders of magnitude in the strong coupling regime. To prove that this 
effect originates from strong matter-light coupling only, experiments were performed with and without an upper mirror. While in a 
microcavity strong RS enhancement was observed, without an upper mirror no notable enhancement was detected. Also, when a cavity 
photon was out of resonance with the molecular vibrational transition (and so when strong coupling was not achieved), no enhancement was 
observed as well. For these reasons it was then concluded that this effect occurs due to strong light-matter 
coupling only. 

To understand the nature of this remarkable effect, we \cite{Raman_our_paper} and a group in Madrid \cite{Raman_Madrid} proposed 
and solved similar models resulting in the same conclusion that vibron-polariton formation does lead to redistribution of RS 
signal between upper and lower polaritons, but does not lead to any notable enhancement of RS cross-section even under 
artificially ultra-strong matter-light coupling. In both papers \cite{Raman_our_paper, Raman_Madrid} the electronic excitation 
of a molecule was treated as a two-level system, while a local molecular vibrational mode (vibron) --- as an harmonic 
oscillator; strong coupling of a fundamental cavity mode and a molecular vibron was the considered in both papers. 
In our paper \cite{Raman_our_paper} we derived an exact analytical expression for the probability of Raman scattering 
at any strength of matter-light coupling with an arbitrary number of molecules. We showed that RS probability 
to the upper polariton goes down and to the lower polariton goes up under increasing polariton Rabi splitting, thus 
concluding that RS enhancement or reduction originates from the softening or ``hardening'' of the final mode respectively. 
While we did find around $100\%$ overall enhancement of RS signal at reasonable experimental conditions, our results 
do not show more than three time RS enhancement even at currently experimentally impossible matter-light coupling, 
which is far below the $10^2 - 10^3$ enhancement reported in \cite{enhanced_raman}. 
In the paper \cite{Raman_Madrid} a different approach based on numerical solution of a small driven-dissipative system 
(with the same system Hamiltonian) was employed. While the authors considered a system of only a few molecules 
below ultra-strong coupling, they did consider not only Hamiltonian, but also driven-dissipative dynamics. However, 
their results are basically identical to ours. 

Overall, the current conclusion is that we do not understand the giant polaritonic RS enhancement observed in 
\cite{enhanced_raman} and so we need a better theory and more experimental checks. However, while two theoretical papers 
already exist \cite{Raman_our_paper, Raman_Madrid}, the giant polaritonic RS enhancement 
was reported in literature only once --- although with quite convincing details --- in one particular setup \cite{enhanced_raman} 
in 2015, which thus hardly motivates further theoretical investments. 

Chapter~\ref{Raman_chapter}, which is based on the results of Ref.~\cite{Raman_our_paper}, discusses the model and the solution in 
details, showing how RS scattering probability evolves from weak to ultra-strong vibron-photon coupling. 










\section{Polariton lasing}
\label{polariton_lasing_intro}

Bose-Einstein statistics is a necessary ingredient of Bose-Einstein condensation \cite{pitaevskii2016bose}, which leads to 
remarkable phenomena such as superconductivity, superfluidity and lasing. Examples of quantum condensates span from liquid $\text{He}_{3,4}$,
superconductors, magnons \cite{demokritov2006bose, giamarchi2008bose}, cold atoms \cite{anderson1995observation} 
to excitons \cite{kogar2017signatures}, Quantum Hall excitons in double-layer structures in strong magnetic fields \cite{eisenstein2004bose}, 
exciton-polaritons in inorganic semiconductors \cite{plumhof2014room} and organic molecules \cite{kena2010room, plumhof2014room} and even photons \cite{klaers2010bose}. 

Bosonic condensation usually requires low temperature to prevent thermal depletion of a condensate or ionization (unbinding) of composite 
bosons (such as Cooper pairs or excitons) and to allow strong overlap of De Broglie waves of constituent bosons. The most prominent 
example is the condensation of cold atoms, which requires temperatures as low as nano-Kelvins. In contrast, exciton-polaritons 
(which are superpositions of a photon and an optically-active matter excitation) in organic materials allow to reach condensation even 
at room temperatures \cite{plumhof2014room}, which is not surprising due to large polariton de Broglie wavelength (because of small mass 
of a photon) and strong binding energy of excitons in organic molecules. Moreover, from the observed experimental results it is seen that 
low temperature may even prevent condensation of polaritons in organic materials by suppressing thermalisation due to freezing out of 
low-energy degrees of freedom, while high temperature activates them and allows thermalisation leading to condensation at high enough 
density of polaritons \cite{plumhof2014room}. 

The manifestations of polariton condensation is straightforward: it is the development of spatial and temporal coherence and non-linear 
increase of photon emission, which signals emergence of a condensate \cite{plumhof2014room, daskalakis2014nonlinear}. Condensation 
is known to break the $\text{U}(1)$ symmetry, which then leads to increased spatial coherence, seen via fringes in interferometric pictures 
of a polariton cloud. The fact that in a condensed regime a single mode is macroscopically 
populated readily leads to very narrow emission spectrum, i.e. to temporal coherence. Another manifistation of polariton condensation is the 
appearance of vortices \cite{lagoudakis2008quantized} (which can be seen as dislocations in interferometric pictures), which try to restore the 
$\text{U}(1)$ symmetric state. Until recently, a blue shift of emission has been believed to be another signature of condensation in organic 
materials and has been associated with repulsive interaction of a macroscopically large number of excitons. However, it is now clear that a blue 
shift in organic polaritons originates simply from the reduction of polariton Rabi splitting upon increasing pumping 
\cite{yagafarov2019origin, betzold2019coherence}, which happens far below the value of external pumping required for polariton condensation. 

In spite of numerous experiments on exciton-polariton condensation (or lasing) with organic materials
\cite{kena2010room, daskalakis2014nonlinear, plumhof2014room, cookson2017yellow, PhysRevLett.115.035301, scafirimuto2017room, rajendran2019low}, 
until recently a detailed picture of the microscopic nature of organic polariton condensation (lasing) mechanism has not been understood.  
A relatively widely accepted story of polariton condensation is that as soon as the number of the lowest energy polaritons exceeds some 
critical value, it stimulates scattering of polaritons from a reservoir, thus leading to low-threshold polariton condensation, which 
does not require electronic inversion, in contrast to ordinary optical lasers. The role of molecular vibrons in the process of polariton 
condensation with organic molecules is believed to assist scattering from a reservoir to the lowest energy polariton mode. 

Although this story sounds reasonable, it immediately leads to a number of questions. For example, how does an ordinary laser at weak 
matter-light coupling, which requires inversion, transform to a polaritonic laser, which is believed to be inversionless? Is there a 
continuous or an abrupt transition between these two regimes? Also, if a molecular vibron is strongly coupled to an electronic 
transition (local intramolecular exciton), then it cannot be considered perturbatively (i.e. as a polariton scattering-assisted mechanism only), 
while it must be treated on equal footing with a molecular exciton coupled to a cavity photon, i.e. a full strongly-coupled 
driven-dissipative vibron-exciton-photon system must be considered. Another question is how to optimise the system parameters, 
such as exciton-photon detuning and polariton Rabi splitting to have as low-threshold lasing as possible? And does a polariton laser 
really outperform an ordinary laser in terms of pumping threshold as widely believed? 

To answer these questions, standard macroscopic approaches used in the physics of polariton condensates based on the complex Gross-Pitaevskii (GP)
equation \cite{PhysRevLett.99.140402, amo2009superfluidity, 10}, which describes evolution of polariton condensate density under pumping, decay 
along with a phenomenological polariton-polariton interaction (which is usually of contact type), are not adequate. Indeed, a phenomenological GP 
equation is universal for any condensates independently on their microscopic details because microscopic degrees of freedom are integrated out and 
phenomenologically included in different incoherent processes to model condensate dynamics. 

In contrast, our aim here is to describe the dynamics of a strongly coupled vibron-exciton-photon system under different 
incoherent mechanisms, to study the interplay of coherent and incoherent dynamics, to uncover lasing mechanism from weak to strong 
matter-light coupling, in other words going from photon to polariton lasing, where the very nature of a condensate changes. 
Therefore, a fully miscroscopical approach is required. 

Chapter~\ref{org_pol_lasing_chapter}, which is based on the results of Ref.~\cite{polariton_lasing_PRL}, introduces a microscopic model 
of $N$ molecules in a microcavity, where each molecule has a single electronic transition (intramolecular exciton) and a vibron 
(local intramolecular vibrational mode) strongly coupled to an exciton. All molecules are coupled to a single cavity mode via 
their electronic transition. Incoherent processes are included within the Markovian second-order perturbation theory, conventionally 
written in terms of a Lindblad master equation \cite{Open_Q_Systems_Breuer_Pettruccione} for a system (vibron-exciton-photon) density matrix. 
The reminder of Chapter~\ref{org_pol_lasing_chapter} provides an exact (in the thermodynamic limit $N \rightarrow \infty$) solution 
of this model and for the first time answers all the questions outlined above. 







\section{Imbalanced polariton condensates}
\label{imb_cond_intro}


\subsection{Fermionic pairing}
\label{imb_cond_intro_fermionic_pairing}

The ground state of non-interacting electrons (and, in general, fermions) is a Fermi sea, where electrons occupy all states with energies 
below the Fermi energy $E_F$. In the second quantized notation, the Fermi sea state is 
$|FS\rangle = \prod_{k < k_F} c^{\dagger}_{k \uparrow} c^{\dagger}_{k \downarrow} | 0 \rangle$, 
where $| 0 \rangle $ is a fermionic vacuum, i.e. $ c_k | 0 \rangle = 0, \, \forall k$ and $k_F = \sqrt{2mE_F/\hbar^2}$ --- Fermi momentum 
($m$ is a particle mass). However, arbitrary small interaction is enough to make a Fermi sea unstable and induce 
superconductivity (the Kohn-Luttinger argument \cite{Kohn_Luttinger}), which --- at the mean-field level --- is described by the BCS wavefunction 
\begin{equation}
\label{BCS_wf_intro}
\prod_{k} ( u_k + \upsilon_k c^{\dagger}_{k \uparrow} c^{\dagger}_{-k \downarrow} )| 0 \rangle,
\end{equation}
which is believed to work well in both strong and weak 
interaction limits \cite{nozieres1985bose}. The phenomenon of fermionic pairing and condensation is universal and goes 
far beyond electronic superconductivity, 
spanning condensation of pairs of cold atoms \cite{bloch2008many}, liquid $\text{He}_3$ \cite{vollhardt2013superfluid}, 
color superconductivity in high-energy physics \cite{color_supercond}, exciton condensation \cite{kogar2017signatures}, 
Quantum-Hall exciton condensation \cite{eisenstein2004bose}, and condensation of exciton-polaritons in microcavities \cite{kasprzak2006bose}. 
However, independently on the microscopic nature of interacting fermions, the mean-field ground state is described by the same wavefunction (\ref{BCS_wf_intro}). 
For example, the condensed state of excitons in a semiconductor with a conduction and a valence band is 
$\prod_{k} ( u_k + \upsilon_k e^{\dagger}_{k, c} e^{\mathstrut}_{k, v} )| 0 \rangle$, where $e_{c/v}$ annihilates an electron in a conduction or a 
valence band. Making a particle-hole transformation of electrons in the valence band, this can be rewritten as 
$\prod_{k} ( u_k + \upsilon_k e^{\dagger}_{k} h^{\dagger}_{-k} )| 0 \rangle$, which is formally identical to the BCS wavefunction (\ref{BCS_wf_intro}). 
This is why the description of excitons in semiconductors and Cooper pairs in superconductors is formally identical.  
The mean-field wavefuntion of a quasi-equilibrium condensate of polaritons has one extra component, namely a coherent state of a photon:
$e^{\lambda a^{\dagger}} \prod_{k} ( u_k + \upsilon_k e^{\dagger}_{k} h^{\dagger}_{-k} )| 0 \rangle$ \cite{BCS_polariton}. 


\subsection{Imbalanced condensates}

While the physics of a simple condensed state is relatively well understood, the interplay of fermionic condensation, e.g. superconductivity, 
with other possible states, such as charge or magnetically ordered states, is a broad and vibrant subject of ongoing research. Of particular 
interest is the possibility of tuning states of matter externally by such standard means as chemical doping and cooling \cite{keimer2015quantum} 
widely used in the field of high $\text{T}_c$ superconductors, or by tuning an external electric field \cite{li2016controlling}, 
by more recent techniques as laser pulse excitation \cite{fausti2011light, K3C60_supercond_first}, or by recently proposed strong 
matter-light coupling for changing matter properties (e.g., \cite{sentef2018cavity, Cavity_Quantum_Eliashberg_SC, wang2019cavity, schlawin2019cavity}). 

A well known example of changing an ordered state of electrons is application of a strong magnetic field to a superconductor. 
Its effect can be modeled by a term 
$h \sum_{k} (c^{\dagger}_{k \uparrow} c^{\mathstrut}_{k \uparrow} - c^{\dagger}_{k \downarrow} c^{\mathstrut}_{k \downarrow})$, 
which favours a polarised state with all electrons spins pointing along the applied field, i.e. it makes a system spin-imbalanced. 
Strong enough magnetic field breaks superconductivity, thus bringing a system from a superconducting to a normal state, which is 
probably the last thing one wants to do. Fortunately, this is not the end of the story. Finite imbalance introduces frustration 
to the system and before ending up in a normal state, the system adjusts itself to stay partially coherent, i.e. to have finite 
density of paired electrons, and at the same time to accommodate finite density of unpaired electrons. The most prominent 
candidate of such state is a so-called FFLO (Fulde-Ferrel-Larkin-Ovchinnikov) state \cite{FF_original, LO_original}, 
which has finite density of paired electrons with finite centre of mass pairing momentum (or a superposition of pairing momenta) and 
also unpaired electrons. This state is proposed to be the ground imbalanced condensed state in many different contexts 
ranging from superconductors and imbalanced QCD systems \cite{casalbuoni2004inhomogeneous} to cold atoms 
\cite{radzihovsky2010imbalanced,parish2007finite} and bi-layer excitons \cite{parish2011supersolidity}. 
Due to simultaneously present bosonic (fermionic pairs) and fermionic (unpaired fermions) degrees of freedom, the FFLO 
state can be considered as a Bose-Fermi mixture, which beyond mean-field fate is yet to be explored, although considerable 
steps have been made \cite{PhysRevLett.103.010404, PhysRevX.4.021012}. 

A new platform for studying imbalanced condensates is currently emerging from a recently developed system of electrically biased 
semiconductor monolayers (a field-effect transistor with a grounded layer being the ``system'') in optical microcavities 
\cite{Polaron_polariton, chernikov2015electrical, chakraborty2018control, dibos2019electrically, fernandez2019electrically}, 
which will allow to experimentally access the physics of imbalanced exciton and exciton-polariton condensates. 
The phase diagram of an imbalanced exciton-polariton system is expected to be much different from, e.g. a phase 
diagram of a superconductor in magnetic field or an imbalanced system of fermionic cold atoms, due to strong long-range 
Coulomb interaction and extra photon-mediated interaction. Due to low mass of a photon, imbalanced polariton condensates 
with pairing wavevector near zero may also be stable at large temperatures akin to a usual polaritonic condensate. 




\subsection{Polaritonic Bose-Fermi mixtures}

While in the field of excitons and exciton-polaritons in inorganic semiconductors, mostly balanced electron-hole systems have been 
studied, there are a few notable exceptions. Firstly, there is the Fermi-Edge singularity (FES) effect in doped semiconductors, 
which manifests itself in strongly enhanced spectral function of electrons near conduction bang edge, thus leading to an absorption 
peak at corresponding interband transition (rather than vanishing or constant absorption in 3D and 2D respectively) 
\cite{nash1988many, PhysRevB.45.8464, PhysRevLett.99.157402, smolka2014cavity}. 
Secondly, there is the physics of trions (bound states of an exciton and an electron or hole) which has been discussed in the 
context of doped or biased semiconductors \cite{PhysRevLett.71.1752, PhysRevB.62.8232, mak2013tightly, yang2015robust}. 
Thirdly, in a pioneering experiment \cite{Polaron_polariton} it was shown that dressing of exciton-polaritons by itinerant 
electrons in biased semiconductors leads to the formation of Fermi-polaron-polaritons. 
It is not difficult to see that all the phenomena mentioned above originate from precisely the same system, which is 
an imbalanced semiconductor (with unequal densities of electrons and holes) and so they probably discuss the same 
physics using different language. Therefore, to understand the physics of imbalanced polaritonic systems and so to 
be able to reliably predict new effects, it is of great importance to develop a unified approach to these systems 
and then to systemise the language of this field to get rid of the present confusion. 

On the other hand, the ground state of a strongly enough (but not too strongly) imbalanced fermionic system is known 
to be the FFLO state. However, so far the transition from the FES/trion/polaron regime to an FFLO regime is not 
well understood (although the first step has been recently made \cite{cotlet2018rotons}), 
so these phenomena remain relatively disconnected. It is the aim of future theory and experiments to unify them. 


Another proposal of polariton-electron Bose-Fermi mixtures includes the effects of interaction of polariton condensate 
in one semiconductor layer with a two-dimensional electron gas (2DEG) in another layer. There are theoretical 
works showing that such a setup can be used to drive high $\text{T}_c$ (up to room temperature) superconductivity in 
2DEG \cite{PhysRevLett.104.106402, PhysRevLett.120.107001}. 
On the other hand, there is a paper showing the formation of a supersolid in the polaritonic 
subsystem \cite{PhysRevLett.108.060401}. 
A recent paper \cite{Supercond_imamoglu} (devoted to unify these phenomena) showed that superconductivity in 2DEG and supersolidity in 
polaritonic condensate are closely intertwined and that superconducting $\text{T}_c$ is expected to be 
of the order of a few kelvins, which also suggests that non-self-consistent approaches applied in Ref.~\cite{PhysRevLett.104.106402, PhysRevLett.120.107001} 
may overestimate $\text{T}_c$. 



\subsection{Imbalanced polariton condensates}

It is the aim of Chapter~\ref{imb_cond_chapter} to provide a firm foundation for studies of imbalanced 
(with different densities of electrons and holes) polaritonic systems, which belong to a more general class of Bose-Fermi mixtures. 
Particularly, an electrically biased layer of a semiconductor in a microcavity is considered and then a finite temperature 
mean-field phase diagram of an imbalanced polariton system is obtained. On the one hand, a number of different phases are   
presented, switching between which may be done by tuning temperature, applied voltage or changing the distance between microcavity 
mirrors. On the other hand, a mean-field phase diagram covers a wide range of system parameters within a single approach and so 
for the first time provides a clear map to study the effects of fluctuations (e.g., such as polaron dressing of polaritons or 
emergence of new phases) and interplay between different phases in search of new effects in imbalanced polaritonic condensates. 








