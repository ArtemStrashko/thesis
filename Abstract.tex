\chapter*{Abstract}

This dissertation studies the effects of strong matter-light coupling on properties of organic molecules 
and inorganic semiconductors. The interplay of complex intramolecular dynamics and strong coupling of a photon 
to molecular transitions results in new physics having no counterparts in other systems. 
In contrast, low-energy optically active excitations of semiconductors (excitons) usually do not feature 
such complexity. However, the combination of strong electronic correlations and strong matter-light coupling 
leads to new physics. 

Firstly, the effect of strong coupling between molecular vibrations and infrared photons on Raman scattering (RS) is considered. 
This is motivated by the experiment of Ref.~\cite{enhanced_raman} showing up to $10^3$ enhancement 
of RS signal under strong coupling. While the exact analytical results of this dissertation predict around $100\%$ enhancement 
of total RS signal, they cannot explain orders of magnitude enhancement, leaving the question open for further studies. 

Next, the effects of strong coupling of an optical photon and a molecular electronic transition on molecular lasing properties 
are discussed. Starting from a microscopic description of a driven--dissipative system, an exact 
(in the thermodynamic limit) mean-field solution is developed. It allows to uncover the mechanism of molecular lasing in the weak 
and strong coupling regime and to obtain a non-equilibrium lasing phase diagram. 

Finally, a semiconductor with different densities of electrons and holes, strongly coupled to a microcavity photon, is studied. 
While finite electron--hole density imbalance is detrimental for excitonic condensation, it may still lead to 
a condensed state of excitons with finite centre of mass momentum coexisting with unpaired electrons. On the other hand, due to its 
low mass, a photon favours zero center of mass momentum condensation. The variational mean-field calculations reveal that the interplay 
of these effects leads to a variety of novel states with coexisting polariton condensate and unpaired electrons. 




\chapter*{Acknowledgements}

Over the last four years I have had a great privilege, luck and pleasure to work under supervision of Jonathan Keeling, 
to whom I wish to express my sincere gratitude. I want to thank him for his incredible support, patience, advice, always 
having time for me, comprehensive emails and in-person discussions, for rigorous scientific approaches, for his remarkable 
QFT lectures, and for opening a door to the world of numerical physics for me.  

I would like to thank Peter Kirton for his invaluable support with scientific computing and for discussing physics with me. 
I would also very much like to acknowledge my close friend Alexander Katrutsa who helped me with Python and 
\LaTeX\ so much. I also want to express my gratitude to the Stack Exchange community, which helps me to resolve most 
of computing issues I have. 

This all would not be possible without the priceless support from EPSRC, which allowed me to work in live in this wonderful 
country, to visit so many different places and to meet such amazing people. This experience shaped and reshaped my personality, 
allowed me to open my eyes wider and to change my mind about many questions, made me much more flexible in science and beyond. 
I wish to express my gratitude to the administrators of CDT, particularly to Julie Massey, Christine Edwards, Wendy Clark and 
Debra Thompson, who made me forget what bureaucracy is and were always ready to help with any issues. 

Over my PhD I have also had a remarkable opportunity to stay in Madrid working with Francesca Marchetti, to whom I wish 
to express my gratitude for hosting me, discussing the physics and making my visit so enjoyable. I also want to thank 
Allan MacDonald, who visited us in St Andrews and hosted us in Austin, for his advise and long thorough discussions 
of physics, from which I leaned a lot. 

My experience would not be so enjoyable without other St Andrews PhD students and postdocs, whom I also want to thank, 
particularly Veronika Sunko, Maja Bachmann, Scott Taylor, Michael Lynch-White, Rhea Stewart, Dainius Kilda, Kristin Arnardottir 
and Peter Kirton. 

I wish to thank my teachers from MIPT, particularly Alexey Barabanov, Yakov Fominov, Nikolay Kirichenko and Andrey Gavrikov 
for their amazing lectures, invaluable seminars and particularly for sharing their passion for physics. I want to thank 
my MIPT friends, with whom we went through MIPT challenges and had a lot of fun together, particularly Alexander Gagloev, 
Evgeniy Gaiduk, Matvey Pochechuev, Anton Baranikov, Timur Yagafarov, Maria Usoltseva and Alexander Katrutsa. 
I also want to thank my school physics teacher Elena Yanovskaya, who ran additional physics classes attended by my school 
friend and myself only, for teaching us to solve difficult problems, for her patience and guidance, and for friendship we 
have since then. 

Finally, I would particularly like to thank my family, my parents for their love and continuous support. 
I would like to give my special thanks to my wife Sofia, who has been my love, colleague and my closest friend 
for the last ten years, for her love, for her support, for sharing my happiness, for her understanding of how 
much science requires. I would also like to thank her for adding another layer of meaning and joy to our life --- 
for our daughter Alice. 



\section*{Funding}

This work was supported by the Engineering and Physical Sciences Research Council, the Scottish Doctoral Training Centre in Condensed Matter Physics 
(grant number EP/L015110/1). 


\section*{Research Data}

Research data underpinning this thesis are available at [DOI]






