\newpage
\chapter{Raman scattering with strongly coupled vibron-polaritons}
\label{Raman_chapter}

The calculations presented in this section are inspired by the experiment \cite{enhanced_raman}, where a new technique of increasing Raman scattering (RS) 
based on 
strong coupling of a cavity photon and molecular vibration mode (vibron) was proposed. This new effect is fundamentally different from the known ways 
of enhancing RS, such as plasmonic (near-field) assisted RS \cite{29,30}, Purcell-enhanced scattering \cite{hummer2016cavity} or stimulated RS \cite{demtroder2013laser}, as it is based on matter-light hybridization rather than on various kinds of stimulated emission. 

In the experiment \cite{enhanced_raman} polyvinyl acetate (PVAc) molecules, which have a symmetric CO bond stretching frequency at 215 meV (wavelength 5.7 $\mu m$), 
were used. The choice of molecules is motivated by the fact that their CO vibrational transition is strong and active both in infra-red (IR) absorption 
and in RS. This allowed to perform two different measurements to characterise the system. To study the physics of strong coupling, these molecules were 
embedded into an Ag metal-metal cavity with fundamental frequency in or out of resonance with a molecular vibron. 

The results of IR absorption measurements demonstrated strong coupling of light with molecular vibrons, i.e. formation of lower and upper vibron-polariton 
modes. While polaritonic Rabi splitting was also detected in the RS signal, it turned out to be around two times larger than in the IR measurements. 
The most striking and unexpected consequence of strong coupling was a giant enhancement (around three orders of magnitude) of the total RS 
cross section. 

It is the aim of this section to model polariton-enhanced Raman scattering. Section \ref{model_Raman} introduces the model and discusses Hamiltonian 
eigenstates. Section \ref{Raman_RWA} presents the details and results of Raman scattering calculations in the rotating wave approximation (RWA) when 
matter-light coupling is not very strong, while Section~\ref{Raman_ultrastrong} presents ultra-strong coupling generalisation and dependence of vibron  
frequency on the electronic state. In Section~\ref{transition_to_higher_excited_states} the results of the probability of Raman transition to 
final higher-excited states are discussed. 

The results of calculations show only moderate enhancement of scattering to the lower polariton mode and suppression of scattering to the upper one. 
While this conclusion agrees well with the results of numerical modelling \cite{Raman_Madrid}, which takes into account incoherent dynamics, 
it strongly contradicts the experimental results \cite{enhanced_raman}. Therefore, the question of the origin of the observed effect remains open. 
The fact that the results \cite{enhanced_raman} are the only existing results of polariton-enhanced RS since 2015 is warning and so hardly motivates 
further theoretical efforts. 





\section{RWA Hamiltonian and its eigenstate structure}
\label{model_Raman}

The experimental setup --- a cartoon of which is presented in Fig.~\ref{cartoon}(b) --- consists of a microcavity containing molecules at the center. 
To model the physics of strong coupling, we consider only a single cavity mode, which is in resonance (or close to resonance) with a vibron 
transition of a molecule. In each molecule only the ground and the first excited electronic states are taken into account, which leads to a two-level 
system description of an electronic subspace. This assumption is reasonable if other transitions are far from the resonance with the frequency 
of excitation laser light and are not strongly couple to light. Both conditions are usually satisfied experimentally. 
Vibrational excitation of a molecule is modeled by a single bosonic mode. Although nuclear potential is not parabolic in 
general, which leads to anharmonicity and so a dense level structure near ionization level, in the vicinity of potential minimum it may replaced by 
a simple parabola. This simplification is reasonable because the physics we want to explore is apparently (and as will be shown below) dominated by 
the lowest vibrational states. On top of this, this assumption allows to obtain closed-form results for scattering probabilities and so to gain 
deeper understanding of the model. The assumption of a single vibrational mode is also reasonable as in a typical experimental absorption or 
emission spectra may be well modeled using a single vibronic mode, which is strongly coupled to an electronic transition. The effect of other 
weakly coupled molecular vibrations may taken into account phenomenologically by introducing electronic state dephasing and thermalisation of 
a strongly coupled vibrational mode \cite{Raman_Madrid} within a master equation approach. 
Also, assuming that direct molecule-molecule coupling is much weaker than cavity-enhanced exciton-photon coupling, the former is neglected. 
This assumption is reasonable for molecules which do not have a static dipole moment, thus which interact via Van der Waals potential. 


\begin{figure}[h!]
\center{\includegraphics[width=4in]{figures/cartoon}}
\caption{a) Energy diagram of coupled vibron-photon system, b) System cartoon}
\label{cartoon}
\end{figure}


Starting from the rotating wave approximation with respect to a cavity mode and vibration excitation interaction, 
the Hamiltonian reads
\begin{equation}
\label{hamilt1}
H = \omega_c \hat a^{\dagger} \hat a + 
    \sum_n 
    \Big{(} 
        \frac 1 2 \omega_e \sigma_n^z + 
        \omega_v [\hat b_n^{\dagger} \hat b^{\mathstrut}_n + \sqrt{S}(\hat b_n^{\dagger} + \hat b^{\mathstrut}_n)\sigma_n^z] +
        G(\hat b_n^{\dagger}\hat a + \hat b^{\mathstrut}_n\hat a^{\dagger}) 
    \Big{)},
\end{equation}
where a sum is over all molecules, $\omega_c$ - cavity photon frequency, $\hat  a$ - a photon annihilation operator, $\omega_e$ - energy of an electronic 
transition, $\omega_v$ - frequency of a molecular oscillation, $\hat b$ - corresponding annihilation operator, $S$ - a Huang Rhys factor, which describes 
coupling between vibrational and electronic modes (a displacement of the vibrational mode in the electronically excited state with respect to the ground state). 
$G$ is a vibration--cavity photon interaction strength, $\sigma_n^z$ is 0 or 1, describing whether the n-th molecule is in its ground or excited electronic state. Raman transitions are caused by external laser field $(\sigma_n^+ + \sigma_n^-)E$, which is treated perturbatively. 

Within the second-order perturbation theory, probability of Raman scattering into a polaritonic mode is given by \cite{39}:
\begin{equation}
\label{mostgeneralprob}
P_{0\rightarrow f} = \left| \sum_n \frac{\langle 0|\hat V_1|n \rangle\langle n|\hat V_2|f \rangle}{(E_n-E_0)-\hbar\omega} \right|^2,
\end{equation}
where $\omega$ is the frequency of the excitation field, $E_0$ is initial state energy, $E_n$ - intermediate state energy. The interaction term is 
$\hat V_1=(\vec{d}\vec{e}_{exc})\sigma_n^-\hat A_{exc}^{\dagger}, \hat V_2=(\vec{d}\vec{e}_{emit})\sigma_m^+\hat A_{emit}$. $\hat A^{\dagger},\hat A$ - 
are (non-cavity) photon creation and annihilation operators, $\vec{d}$ - vibron dipole moment, $e$ - polarisation of emitted/absorbed light. 
Polarisation effects are not important in the current context, thus will be ignored hereafter. 


In order to calculate a sum over all virtual transitions in (\ref{mostgeneralprob}), we need to find Hamiltonian (\ref{hamilt1}) eigenstates. 
In general, this is a non-trivial problem requiring extensive numerical calculations. However, what we want to calculate is a probability of a 
spontaneous Raman scattering in a linear-spectroscopy regime. This means that the system contains either one or no electronically excited 
molecules, which simplifies calculations significantly. In a state where only an $m^{th}$ molecule is electronically excited, the Hamiltonian (\ref{hamilt1}) 
reads 
\begin{equation}
H_\text{eff,m} = 
    \omega_c \hat a^{\dagger}\hat a + 
    \sum_n 
    \Big{(} 
        \omega_v \hat b_n^{\dagger}\hat b^{\mathstrut}_n + 
        G(\hat b_n^{\dagger}\hat a + \hat b^{\mathstrut}_n\hat a^{\dagger}) 
    \Big{)} + 
    \omega_v\sqrt{S}(\hat b_m^{\dagger} + \hat b^{\mathstrut}_m).
\end{equation}
The eigenstates of this Hamiltonian play the role of intermediate states $|n\rangle$ in (\ref{mostgeneralprob}). 
This Hamiltonian is readily diagonalized by the unitary transformation $ \hat \xi_i=\hat a\upsilon_i+\sum_n \hat b_n U_{n,i} + \beta_{m,i} $ 
(requiring $[\hat \xi_i,\hat \xi_j^{\dagger}]=\delta_{i,j}$). This leads to $N+1$ eigenmodes: 2 polaritonic ones and $N-1$ dark modes 
(which are orthogonal to polaritonic ones and do not contain photonic part, i.e. for which $\upsilon_i \equiv 0$).

Polaritonic mode energies are:
\begin{equation}
\omega_i\equiv\omega_{1,2}=\frac{\omega_c+\omega_v}{2}\pm \sqrt{\Big{(}\frac{\omega_c-\omega_v}{2}\Big{)}^2 + NG^2},
\end{equation}
This expression suggests a definition of Rabi splitting as a splitting between upper and lower polariton at resonance $\Omega_R = 2 G \sqrt{N}$. 
Then using the requirement $[\hat \xi_i,\hat \xi_j^{\dagger}]=\delta_{i,j}$, the fraction of a photon and exciton in the polaritonic mode are:
\begin{equation}
\label{upsdown}
\upsilon_i=\frac{\omega_i - \omega_v}{\sqrt{(\omega_i-\omega_v)^2+NG^2}},
\end{equation}
\begin{equation}
\label{Upol}
U_{n,i}\equiv U_i=\frac{G}{\sqrt{(\omega_i-\omega_v)^2+NG^2}}.
\end{equation}
A Hamiltonian without electronically excited molecules is diagonalised by the same transformation, but with $\beta_{m,i} = 0$, i.e. 
$ \hat \eta_i=\hat a \upsilon_i + U_i \sum_{n}\hat b_n $, 
and so it leads to the following relation between annihilation operators for intermediate (with an electronically excited molecule) 
and initial or final (all molecules in their electronic ground state) states: $ \hat\eta_i+\beta_i = \hat\xi_i $.


Let us now consider the remaining $N-1$ solutions, i.e. dark modes ($\upsilon_i \equiv 0$) with $\omega_{3,4,...,N+1}=\omega_v$ and 
$\hat\xi_{m}=\sum_{n}U_{n,m}\hat b_m$ with $U_{n,m}$ such that $\sum_{n}U_{n,i} = 0$, $ \sum_{n}U_{n,i}^{}U^*_{n,j}= \delta_{i,j} $.
Firstly, it is clear that there are not enough equations to determine eigenvectors unambiguously, so there are many ways to chose them. 
However, observables cannot depend on the specific form of eigenvectors, so we should just choose a convenient set of eigenvectors. 
For example, $U_{n,m} =\frac{1}{\sqrt{N}} \exp \left( i\frac{2\pi nm}{N}\right)$, where
$n=0,...,N-1$ - the number of a molecule, $m=1,...,N-1$ - the number of a dark mode.

The last ingredient $\beta_{j,m}$ is given by 
\begin{equation}
\label{betatotal}
\beta_{j,m} =
\begin{cases}
\frac{U_j\omega_v\sqrt{S}}{\omega_j}, & \text{j=1,2\; (or UP and LP)}, \\
U_{j,m}\sqrt{S}, & \text{j=3,4,...,N+1},
\end{cases}
\end{equation}
where m is a number of an electronically excited molecule, j - the mode number. For polaritonic modes $\beta_{j,m}$ does not depend on the number 
of a molecule $m$ because polariton is delocalised over all molecules, i.e. it is a superposition of a photon and a delocalised excitation of 
all molecules. 


Now the ground state of a Hamiltonian without electronically excited molecules is 
$ |0\rangle=|\Downarrow,0_L,0_U,0_1,...,0_{N-1}\rangle $  
and an intermediate state with an excited molecule is 
$ |0_{int,j}\rangle=|\Uparrow_j,0_L,0_U,0_1,...,0_{N-1}\rangle $ 
($\Uparrow_j$ means electronically excited $j$-th molecule, the first number --- number of lower polaritons, second --- upper polaritonic, 
all others --- numbers of dark states). Due to the relation $ \tilde{\eta}_j + \beta_{j,m} = \tilde{\xi}_j $,
the connection between these two is  
\begin{equation}
|0_{int,j}\rangle = e^{-\sum_i \left( \beta^{\mathstrut}_{j,i}\hat \eta_i^{\dagger} - \beta_{j,i}^*\hat \eta^{\mathstrut}_i\right) } |0\rangle.
\end{equation}






\section{Transition probability calculation in RWA}
\label{Raman_RWA}

Transition probability from the ground to the final state via all virtual states is $P_{|0\rangle \rightarrow |f_k\rangle} = \gamma \left| M_k \right|^2$ 
($\gamma$ combines various factors, which do not depend on matter-light interaction strength, such as 
an electronic matrix element and density of final photon states), where a transition matrix element is 
\begin{equation}
\label{mk}
M_k = \sum_{n} \frac{\langle 0 | \sigma_n^-|n\rangle\langle n | \sigma_n^+|f_k\rangle}{ (E_{int, n} - E_{0} - \hbar\omega)}. 
\end{equation}
In this expression $|f_k\rangle$ is the final state (index $k$ labels which state is excited), which may be an upper (UP) or lower (LP) polariton mode 
or a dark state, $E_{int, n}$ is the energy of a virtual (intermediate) state


Operators $\sigma_n$ only change the electronic state of an $n$-th molecule, so now we can write
\begin{equation}
\label{prelimprob}
M_k =
    \sum_{\substack{n;\\p_1,\dots,p_{N+1}}}
    \frac{ e^{- \sum_i|\beta_{n,i}|^2} \mathpzc{M}_{int,0}(n;p_1,\dots,p_{N+1}) \mathpzc{M}_{f_k,int}(n;p_1,\dots,p_{N+1})}
    {p_1!\dots p_2! \Big{(} E_{el} - \hbar\omega + p_1\omega_{UP} + p_2\omega_{LP} + \omega_v(p_3+\dots + p_{N+1})  \Big{)}} ,
\end{equation}
where $p_i$ describes the number of excitations in a corresponding polariton or dark mode, $E_{el}$ - energy of the electronic transition, $\omega_{UP}$ and $\omega_{LP}$ are the UP and LP frequencies respectively. Also, $\omega_v$ is the energy of a dark mode. 




Now let us consider matrix elements in (\ref{prelimprob}). The first matrix element is trivial:
\begin{equation}
\label{first_Raman_matrix_element}
\begin{split}
\mathpzc{M}_{int,0} &= \langle 0 | (\hat \eta_1^{\dagger} + \beta_1)^{p_1} (\hat \eta_2^{\dagger} + \beta_2)^{p_2} \dots
(\hat \eta_{N+1}^{\dagger} + \beta^*_{n,N+1})^{p_{N+1}} e^{-\sum_i\beta_{n,i}\eta_i^{\dagger}} | 0 \rangle = \\&
= \beta_1^{p_1}\beta_2^{p_2}(\beta^*_{n,3})^{p_3} \dots (\beta^*_{n,N+1})^{p_{N+1}}.
\end{split}
\end{equation}
Here $\beta_{1/2}$ are real as they refer to polaritonic states (see Eq.~\ref{Upol}). 
The second matrix element describing the transition from the intermediate state to one of $N+1$ single excited (polariton or dark) states 
in electronic ground state is:
\begin{equation}
\label{second_Raman_matrix_element}
\mathpzc{M}_{f_k,int} = \mathpzc{M}_{int,0}^*\frac{p_k-|\beta_{n,k}|^2}{\beta_{n,k}}.
\end{equation}
The difference between these matrix elements comes simply from the fact that the first one (\ref{first_Raman_matrix_element}) originates from 
an expression like $\langle 0 | (\xi^{\dagger} + \beta^*)^p e^{-\beta \xi^{\dagger}} | 0 \rangle$, while the second one (\ref{second_Raman_matrix_element}) 
-- from $\langle 0 | \xi (\xi^{\dagger} + \beta^*)^p e^{-\beta \xi^{\dagger}} | 0 \rangle$. 
Now we can rewrite Raman transition matrix element (\ref{prelimprob}) in a more compact form:
\begin{equation}
\label{sumcalc}
M_k =
    \sum_{n}
    \frac{1}{\beta_{n,k}} 
    \sum_{\{p_i\}} 
    \prod_{i} 
    e^{-|\beta_{n,i}|^2} 
    \frac{|\beta_{n,i}|^{2p_i}}{p_i!} 
    \frac{p_k - |\beta_{n,k}|^2}{\Delta + \sum_j p_j\omega_j},
\end{equation}
where $\Delta=E_{el} - \hbar\omega$ is the laser field--electronic transition energy detuning and $\sum_{\{p_i\}} \equiv \sum_{p_1,..., p_{N+1}}$. 
As energy levels can be degenerate (dark modes), calculation of a scattering probability with a definite Stokes shift, requires summation over all 
final states with the same energy, i.e.
\begin{equation}
\label{sumdegen}
P(\nu)\propto \sum_k\delta(\nu-\omega_k)P_{|0\rangle\rightarrow |f_k\rangle}.
\end{equation}
However, this should be applied only for dark modes because polaritonic modes are not degenerate.


Summation over the number of excitations of all modes $\{p_i\}$ in (\ref{sumcalc}) can be performed analytically. 
To do this, we can rewrite the denominator as an integral of an exponential and get
\begin{equation}
\label{likein}
\begin{split}
M_k &= \int\limits_0^{\infty}dz e^{-z \Delta} \sum_{n} \frac{1}{\beta_{n,k}}
      \sum_{\{p_i\}} \left( p_k - |\beta_{nk}|^2 \right) \times
      \left( \prod_j \frac{\left( |\beta_{nj}|^2 e^{-z \omega_j} \right)^{p_j} e^{-|\beta_{nj}|^2}}{p_j!}
      \right)
       = \\& =
		      \int\limits_0^{\infty} e^{-z \Delta} \sum_{n} \frac{\left( e^{-z \omega_k} - 1 \right)}{\beta_{n,k}}
		      |\beta_{nk}|^2 \prod_j e^{ |\beta_{nj}|^2 \left( e^{-z \omega_j} - 1 \right) },
\end{split}
\end{equation}
thus we get
\begin{equation}
\label{rw1}
M_k = \int\limits_0^{\infty}dz e^{-z \Delta} \sum_{n} \beta_{n,k}^*
      \left( e^{-z \omega_k} - 1 \right)
      \exp \left[ \sum\limits_{j} |\beta_{nj}|^2 \left( e^{-z \omega_j} - 1 \right) \right].
\end{equation}
This expression together with (\ref{sumdegen}) and a dark mode condition $\sum_n \beta_{ni} = 0$ 
clearly shows that there is no transition to a single-excited dark state, which has a simple interpretation. 
Indeed, dark state is a highly degenerate state, so the transition matrix elements to every dark state is the same up to a phase factor, 
therefore if we consider a transition to the dark state, we should sum up over all transition amplitudes to the dark states, which cancel 
due to destructive interference.

For the bright states $\beta_{n,k}$ does not depend on $n$ (and it is real), so for the transition to polaritonic states we have $\sum_n \rightarrow N$, 
where $N$ is a total number of molecules. Also, for dark states $|\beta_{n,j}|$ does not depend on $j$, therefore $\sum_{j\in\text dark} \rightarrow N-1$ 
or just $N$ for a large number of molecules. So, we get the final expression for the transition amplitude to the polaritonic mode 
\begin{equation}
\label{MkRWA}
M_k = \frac{N\alpha_k}{\sqrt{2}}  \int\limits_0^{\infty}dz e^{-z \Delta}
      \left( e^{-z \omega_k} - 1 \right)
      \exp  \left[ -\frac 1 2 \sum\limits_{j} |\alpha_{nj}|^2 \left( 1 - e^{-z \omega_j} \right) \right],
\end{equation}
where for polariton transition $k=1$ or $3$; $i=1,2,3$ and in resonance $\omega_c = \omega_{\upsilon}$ we have 
$\alpha = \sqrt{\frac{S}{N}}\omega_v \left(\frac{1}{\omega_{UP}}, \frac{\sqrt{2N}}{\omega_v}, \frac{1}{\omega_{LP}}  \right)$.

\begin{figure}[h!]
\center{\includegraphics[width=3.2in]{figures/RWA}}
\caption{Transition probability to the upper and lower polariton in RWA; $\omega_c = \omega_v$, $S = 0.3$, 
            $N = 10^6$, and $\Delta = \omega_v$. }
\label{RWAfig}
\end{figure}


Fig.~\ref{RWAfig} shows Raman scattering probability (normalized by the probability in the absence of light-matter coupling) 
as a function of dimensionless coupling (half Rabi splitting $G \sqrt{N} = \Omega_R/2$ divided by the bare vibron frequency $\omega_v$). 
As RWA approximation is valid only at quite small coupling strength, we can conclude that in this limit the total Raman scattering 
cross-section is almost unaffected by the strong vibron-cavity photon coupling.


Fig.~\ref{det} demonstrates the dependence of Raman scattering probability on detuning $(\omega_c - \omega_v) = \delta$. 
Thick lines on this figure describe the scattering into the 1st and 2nd polaritonic mode for a half Rabi splitting $\Omega_R=20meV$,
dashed - for $\Omega_R=5meV$. So, the signal just splits between polaritonic levels.

\begin{figure}[h!]
\center{\includegraphics[width=3.2in]{figures/detuning}}
\caption{Transition probability to the LP and UP dependence on the vibron-cavity photon detuning in RWA. 
            Red - lower polariton, blue - upper polariton; solid line - $G\sqrt{N} = 0.1 \omega_v$, dashed - $G\sqrt{N} = 0.2 \omega_v$}
\label{det}
\end{figure}




If in (\ref{MkRWA}) we suppose a large laser field--electron excitation energy detuning $s \ll \Delta$, we can notice that only small 
values of $z$ contribute to the integral, so we can expand exponents and keeping only first non-vanishing terms calculate the integral. 
For the corresponding transition probability to the UP or LP we get
\begin{equation}
\label{rwatrapprox}
P_{1,2} \simeq   \gamma  \frac{S N}{2}
              \frac{\omega_v^2}{\left[
              \Delta + \frac{S}{N} \omega_v \left(
              N+1 + \frac{ 1 }{ 1 - \zeta^2} \right)
              \right]^2},
\end{equation}
where $\zeta = G\sqrt{N}/\omega_v$,
thus it almost does not depend on $\zeta$ for $\zeta \ll 1$, but starts getting smaller at $\zeta > \zeta_0$, where
$ \zeta_0 \simeq 1 - \frac{1}{2N} $, i.e. when divergent part of the denominator dominates on a big but non-divergent part. 
This means that for a macroscopic number of molecules, we reach this regime only at $G\sqrt{N} \simeq \omega_v$, and eventually 
the transition probability vanishes at $\zeta = 1$, i.e. at $G\sqrt{N} = \omega_v$.
However, at such strong coupling RWA is not valid. It is simple to further expand (\ref{MkRWA}) to get a more accurate analytical 
expression, but, as we will see below, (\ref{rwatrapprox}) already contains an important result, namely that the transition probability 
depends weakly on the coupling strength in RWA and that the total signal is almost independent on light-matter coupling.









\section{Ultra-strong coupling \& $\omega_v$ dependence of the electronic state}
\label{Raman_ultrastrong}

In the ultra-strong coupling regime RWA breaks down, so we should change $G(\hat b_n^{\dagger}\hat a + \hat b_n\hat a^{\dagger})$ by an exact expression
$G(\hat b_n^{\dagger} + \hat b_n)(\hat a^{\dagger} + \hat a)$ and add a diamagnetic $\hat A^2$ term, i.e. $\frac{G^2N}{\omega_v}(\hat a^{\dagger} + \hat a)^2$.
The prefactor of this term can be derived following the Thomas-Reiche-Kuhn sum rule (for details see \ref{A2_term_append}). 
Also, using $\hat x = \sqrt{\frac{\hbar}{2m\omega}}\left( \hat b^{\dagger}_n + \hat b_n \right)$ for the molecular oscillator coordinate, 
we can include the term originating from the vibration frequency difference in the ground and excited electronic state $\delta\omega_v$. 
It should be noted that we would get the term of the same form if also considered the second order expansion in exciton-vibron coupling over oscillator 
displacement (but we will show that this does not play a big role in Raman scattering). Overall, we have
\begin{equation}
\begin{split}
\label{hamilt2}
H  & =  \; \omega_c \hat a^{\dagger}\hat a + \sum_n \Bigg{(} \frac 1 2 \omega_e \sigma_n^z + \omega_v [\hat b_n^{\dagger}\hat b^{\mathstrut}_n +  \sqrt{S}(\hat b_n^{\dagger} + \hat b^{\mathstrut}_n)\sigma_n^z] + \\& +
G(\hat b_n^{\dagger} + \hat b^{\mathstrut}_n)(\hat a^{\dagger} + \hat a) +
\frac 1 2 \sigma_n^z \nu (\hat b_n^{\dagger} + \hat b_n^{\mathstrut})^2 \Bigg{)} + \frac{G^2N}{\omega_v} (\hat a^{\dagger} + \hat a)^2,
\end{split}
\end{equation}
where $\nu = \frac 1 4 \omega_v \left[ 2 \frac{\delta\omega_v}{\omega_v} + \left( \frac{\delta\omega_v}{\omega_v} \right)^2 \right]$


In contrast with the case considered before (\ref{hamilt1}), the new Hamiltonian includes non number conserving terms, 
therefore the Fock basis is not convenient here as it causes significant difficulties in matrix elements calculations. 
At the same time, the Hamiltonian is still quadratic, so it can be diagonalized analytically. However, even before it we can simplify the 
Hamiltonian considerably, rewriting it in terms of three modes, which is explained in the Appendix \ref{appendA}. 
The main idea is that we have 2 polaritonic and N-1 identical dark modes, so there is a way to change the basis in order to have only 3 distinct 
modes (one photonic and two vibrational modes, one of which --- will be called $b$ below --- describes vibrational excitation on the m-th molecule and 
other --- excitation delocalized over $N-1$ molecules except the m-th one, which will be called $c$). 
Therefore, we will get a Hamiltonian which describes three coupled Harmonic oscillators with different frequencies. 


We want to connect eigenstates of a Hamiltonian when we have an electronically excited molecule $H_{\uparrow}$ and a Hamiltonian with 
all molecules in an electronically ground state $H_{\downarrow}$:
\begin{equation}
\label{three}
  H_{\downarrow} = \omega_c \hat a^\dagger \hat a
  + \omega_v (\hat b^\dagger \hat b + \hat c^\dagger \hat c  ) + \frac{G^2 N}{\omega_v}(\hat a + \hat a^\dagger)^2
   +
   (\hat a + \hat a^\dagger)\left(\hat b + \hat b^\dagger + \sqrt{N-1}\left( \hat c + \hat c^\dagger \right) \right)
\end{equation}
and
\begin{displaymath}
  H_{\uparrow} = H_{\downarrow} + \delta (\hat b + \hat b^\dagger)^2
  + \omega_v \sqrt{S} (\hat b + \hat b^\dagger).
\end{displaymath}


As the Fock basis makes it difficult to calculate overlappings, but the Hamiltonian is quadratic, spatial and momentum representation is more appropriate 
in this situation. We can now introduce coordinates $\hat x_i$, and corresponding momentum $\hat p_i$ and (${\hbar=1}$) we write:
$ {\hat a = \sqrt{\frac{\omega_c}{2}} \left(
    \hat x_a + \frac{i \hat p_a}{\omega_c} \right)}$,
 ${ \hat b = \sqrt{\frac{\omega_v}{2}} \left(
    \hat x_b + \frac{i \hat p_b}{\omega_v} \right)},
$
and similarly for $c$. As the problem is isotropic in momentum, we can diagonalize it by solving the classical problem.  If we write:
\begin{equation}
  \hat H_\sigma =
  \frac{1}{2} \left( \mathbf{p}^{\dagger} \mathbf{p} +
    \mathbf{x}^{\dagger} \mathbf{V}_\sigma \mathbf{x} + 2 \mathbf{h}^{\dagger}_\sigma \mathbf{x}
  \right),
\end{equation}
where $\mathbf{h} = \left( 0, \omega_v \sqrt{2 \omega_v S}, 0  \right)$ and
\begin{equation}
\mathbf{V}_{\downarrow} =
\begin{pmatrix}
\omega_c^2 + 4 G^2 N & \xi & \xi\sqrt{N-1} \\
\xi & \omega_v^2 & 0 \\
\xi\sqrt{N-1} & 0 & \omega_v^2
\end{pmatrix}
\end{equation}
\begin{equation}
\mathbf{V}_{\uparrow} = \mathbf{V}_{\downarrow} +
\begin{pmatrix}
0 & 0 & 0 \\
0 & 4\varepsilon_v \nu & 0 \\
0 & 0 & 0
\end{pmatrix}
\end{equation}
where $\xi = 2G\sqrt{\omega_v\omega_c}$, we can clearly diagonalize $\hat H_\sigma$ by writing:
$
  \label{eq:2}
  \mathbf{x} = \mathbf{U}_\sigma \mathbf{X}_\sigma - \mathbf{V}_\sigma^{-1} \mathbf{h}_\sigma
$
where $\mathbf{U}^\dagger_\sigma \mathbf{V}_\sigma
\mathbf{U}_\sigma=\Omega^2_{\sigma}$ is diagonal. 
Note that $\mathbf{V}$ is a real symmetric matrix, and so although we write
Hermitian conjugates, these are all equivalent to transposes, as
eigenvalues and vectors are all real. After diagonalization one has the eigenstates, which are products of three harmonic oscillator 
wave-functions:
\begin{equation}
\label{Raman_WF}
\begin{split}
\Psi_{lmn,\sigma}(x_a,x_b,x_c) &=
   \sqrt[4]{\omega_{1,\sigma}\omega_{2,\sigma}\omega_{3,\sigma}}
  \psi_l\left(X_{1,\sigma} \sqrt{\omega_{1,\sigma}} \right) \\&
  \psi_m\left(X_{2,\sigma} \sqrt{\omega_{2,\sigma}} \right)
  \psi_n\left(X_{3,\sigma} \sqrt{\omega_{3,\sigma}} \right),
\end{split}
\end{equation}
where $\omega_{i\sigma}$ are the diagonal elements of $\Omega_\sigma$ and the components $X_i$ are related to $x_i$ by
Eq.~(\ref{eq:2}) and $\psi_n(x)$ are Gauss-Hermite functions:
\begin{equation}
  \psi_n(x) =
  \frac{1}{\sqrt{\sqrt{\pi} 2^n n!}} H_n(x) e^{-x^2/2}
\end{equation}
Using the fact that $\mathbf{h}_\downarrow=\mathbf{0}$ we can relate
the two basis coordinates as:
$
  \mathbf{X}_\downarrow
  =
  \mathbf{U}_\downarrow^\dagger\left[
    \mathbf{U}_\uparrow \mathbf{X}_\uparrow - \mathbf{V}_\uparrow^{-1} \mathbf{h}_\uparrow
  \right].
$



Now, as in (\ref{mk}), we need to calculate $M_k$, which is a sum over all transition amplitudes from the ground to final state over all intermediate states 
divided by corresponding energy differences. The transition matrix elements can now be calculated as overlaps of position-basis wavefunctions 
(\ref{Raman_WF}). Rewriting the denominator as an integral of the exponent as before, for a transition matrix element to a single excited bright 
(polaritonic) mode $k$ we get
\begin{equation}
\label{summ}
\begin{split}
& M_k  =
    N \sqrt{2 \omega_{k,\downarrow} \prod_i \omega_{i,\uparrow}\omega_{i,\downarrow}}
    \int_0^{\infty} ds e^{-s \Delta}
    \int d^3x d^3x' \\ & 
    \prod_i 
    \Bigg{[} 
        \psi_0 \left( \sqrt{\omega_{i,\downarrow} } X_{i,\downarrow} \right)
        \sum_{l_i}  \psi_{l_i} \left( \sqrt{\omega_{i,\uparrow} } X_{i,\uparrow} \right)
        \psi_{l_i} \left( \sqrt{\omega_{i,\uparrow} } X'_{i,\uparrow} \right)
        \psi_0 \left( \sqrt{\omega_{i,\downarrow} } X_{i,\downarrow}' \right) 
        e^{-s l_i \omega_{i,\uparrow}} 
        \Bigg{]}
        X_{k,\downarrow}
\end{split}
\end{equation}
where a factor of $N$ came from the summation over the number of molecules and a coordinate $X_{k,\downarrow}$ 
came from the relation between the first excited and ground Hermite modes $\psi_1(x) = \sqrt{2} x \psi_0(x)$. 
The coordinate integrals in (\ref{summ}) can be calculated by first making a unitary transformation of coordinates using 
the relation between $\vec{x}$ and $\vec{X}_{\sigma}$ and then identifying known overlaps of Gauss-Hermite functions 
(for further details of calculations see the Appendix \ref{appendB}). The result is
\begin{equation}
\label{final}
\begin{split}
M_k & = 8 N \sqrt{2 \omega_{k,\downarrow}} \left[ \mathbf{U}_{\downarrow}^{\dagger}\mathbf{U}_{\uparrow} \right]_{kr}
      \int ds e^{-s \Delta}
      \prod_i \left( \sqrt{\frac{\omega_{i,\downarrow}\omega_{i,\uparrow}}{1 - \exp(-2s \omega_{i,\uparrow})}} \right) \times \\& \times
      \frac{(\mathbf{A}^{-1}\mathbf{q} - \mathbf{l})_r}{\sqrt{\det(\mathbf{A})}}
      \exp\left( \frac 1 2 \mathbf{q}^\intercal \mathbf{A}^{-1} \mathbf{q} - \mathbf{l}^\intercal \mathbf{R} \mathbf{l} \right),
\end{split}
\end{equation}
where
\sbox0{$\begin{matrix}1&2&3\\0&1&1\\0&0&1\end{matrix}$}
\begin{equation}
\mathbf{A} =
      \left(
      \begin{array}{c|c}
      \vphantom{\usebox{0}}\makebox[\wd0]{\large $\mathbf{P + R}$ }&\makebox[\wd0]{\large $- \mathbf{Q}$}\\
      \hline
        \vphantom{\usebox{0}}\makebox[\wd0]{\large $- \mathbf{Q} $}&\makebox[\wd0]{\large $\mathbf{P + R}$}
      \end{array}
      \right),
\end{equation}
$
\mathbf{R} = \mathbf{U}_{\uparrow}^\intercal \mathbf{U}_{\downarrow} \mathbf{\Omega}_{\downarrow} \mathbf{U}_{\downarrow}^\intercal \mathbf{U}_{\uparrow},
$
$
\mathbf{P} = \diag\left(\frac{\omega_{i,\uparrow}}{\tanh(s\omega_{i,\uparrow})} \right)
$ and
$
{ \mathbf{Q} =  \diag\left(\frac{\omega_{i,\uparrow}}{\sinh(s\omega_{i,\uparrow})}\right)},
$
$
\mathbf{l} = \mathbf{\Omega}_{\uparrow}^{-2}\mathbf{U}_{\uparrow}^{\dagger}\mathbf{h}_{\uparrow},
$
$\mathbf{q}^\intercal = (\mathbf{l}^\intercal \mathbf{R}, \mathbf{l}^\intercal \mathbf{R})$. This result contains a $6\times6$ matrix $A$, 
which naturally comes after computing the $6$ dimensional Gaussian integral in (\ref{summ}) over coordinates of both 3D oscillators.


From Fig.~\ref{delta50} we can see that the detuning between upper and lower vibration energies $\delta \omega_v$ does not play a big 
role in Raman scattering, but non-RWA and $A^2$ terms are crucial at large coupling strength, which is clear from the comparison with the 
results of RWA calculations --- Fig.~\ref{RWAfig}. Therefore, we can conclude that changes in the ground electronic state of the molecule 
due to the strong coupling with light primarily affect the properties of Raman scattering, but transformations in the intermediate states 
affect RS negligibly.

\begin{figure}[h!]
\center{\includegraphics[width=3.2in]{figures/delta50}}
\caption{Probability of the transition to the upper and lower polariton beyond RWA, including $A^2$ term. 
            Thick lines correspond $\delta \omega_{v} = 0$, thin ones --- to  
            $\delta\omega_v=-0.5 \, \omega_v$}
\label{delta50}
\end{figure}



Thus, let us then concentrate on a situation when the detuning $\delta\omega_v$ is zero. In this case the general expression (\ref{final}) 
can be simplified considerably because $\nu=0$, so $V_{\uparrow} = V_{\downarrow}$, consequently $U_{\uparrow} = U_{\downarrow}$ and 
$\Omega_{\uparrow} = \Omega_{\downarrow}$. If so, we get exactly the same expression as (\ref{MkRWA}), but
with a new $\alpha_j$ (again in resonance $\omega_c = \omega_v$): 
$\alpha_j  = 
    \sqrt{\frac{S}{N}} \omega_v^{3/2} 
    \left( 
        \frac{1}{\omega_{UP}^{3/2}}, \frac{\sqrt{2N}}{\omega_v^{3/2}}, \frac{1}{\omega_{LP}^{3/2}} 
    \right)$.

As before, the integrand contributes to the integral only at $s \ll \Delta$, so we can suppose very large detuning $\Delta$, 
and expand exponents. If we do it, we can clearly see that $M_{1,2}$ does not have singularities, but $M_3 \equiv M_{LP}$ 
(transition amplitude to LP) has, so let us consider the corresponding transition probability
\begin{equation}
\label{singexpr}
P_{LP} \simeq 
    \frac{SN}{2} 
    \frac{\omega_v^3}{\omega_{LP}} 
    \frac{1}{\left[ \Delta + \frac{\omega_v^3 S}{2N} \left( \omega_{UP}^{-2} + \omega_{LP}^{-2} + 2N\omega_v^{-2} \right)  \right]^2},
\end{equation}
At relatively small values of matter-light coupling, $P_{LP} \propto 1 / \omega_{LP}$ goes up when coupling increases because of softening 
of the lower polariton mode. This is different from the result obtained in RWA (\ref{rwatrapprox}),
where we did not have such a prefactor. In contrast, at very strong coupling, where 
$\omega_{LP} \ll \omega_v / \sqrt{N}$, the probability goes down as $P_{LP} \propto \omega_{LP}^3$. 
However, for an experimentally reasonable number of molecules $N \approx 10^6 - 10^{10}$ this regime of a ``super-strong'' coupling 
is hardly achievable. On the other hand, in a context of single strongly-coupled emitters 
(for example, in \cite{Single_molec_strong_coupling} single molecule exciton-plasmon strong coupling was demonstrated), 
this effect may arise. 


It should be noted that the $A^2$ term plays a crucial role at ultra-strong coupling regime because it prevents the LP energy 
from abrupt plunge to zero (i.e. from a quantum phase transition to a superradiant state). With this term, LP energy approaches 
zero asymptotically as coupling increases, so Raman probability is a smooth function of the coupling strength. 




\section{Raman scattering to higher-excited states}
\label{transition_to_higher_excited_states}

While transition probabilities to only single excited states (one lower or upper polariton) were derived in previous sections, 
a more general formula describing the transition amplitude to the n-th UP or LP state can be obtained:
\begin{equation}
\label{npoltransit}
M_k^{n} = N \frac{\alpha_k^n}{\sqrt{2^nn!}} \int ds e^{-s \Delta}
      \left( e^{-s\omega_k} - 1 \right)^n
      \exp\left[ - \frac 1 2 \sum_i |\alpha_i|^2 \left( 1 - e^{-s \omega_i} \right)   \right].
\end{equation}
From this formula we can see that scattering probability to the LP state increases faster (as $1/\omega_{LP}^n$ comparing with (\ref{singexpr})) 
when coupling grows. This result is not surprising, because we can guess it qualitatively as the probability of the simultaneous excitation of 
two modes equals to the product of corresponding probabilities (if these are independent).
However, it is also damped by a factor of $1/N^n$ (which comes from $\alpha_k^{2n}$) as in this case we create $n$ delocalized 
excitations over all $N$ molecules.



The most general formula for a transition amplitude to a state $\{ q_i \}$ reads 
\begin{equation}
\label{npoltransit}
M_{ \{ q_i \} } = 
    \sum_m
    \int_0^{\infty} 
    ds e^{- s \Delta} \prod_i (\alpha_{m,i}^*)^{q_i} 
    \frac{(e^{- s \omega_i} - 1)^{q_i}}{\sqrt{q_i!}}
    e^{- |\alpha_{m,i}|^2 (1 - e^{- s \omega_i})}
\end{equation}
This result is quite interesting because it shows that dark states can be excited as a result of Raman scattering because, while 
$\sum_m \alpha_{m,i} = 0$, the sum $\sum_m (\alpha^*_{m,i})^{q_i}$ is not necessarily zero. Indeed, the reason of vanishing probability 
of scattering to a single-excited dark state is a momentum conservation, i.e. as the ground state momentum is zero, the final state 
momentum must also be zero, while dark states have finite momentum (thus they do not couple to a zero-momentum photon). However, 
a final state with two excited dark modes with opposite momenta is possible. In general, any final state is possible as soon as its 
total momentum is zero. Considering all final states with two excitations, one can see that a corresponding transition probability 
scales like $N$, i.e. the same as for a single-excitation final state. Therefore, in the strong coupling regime in the Raman emission 
one should see three peaks: a strong one due to a transitions to a single-excited LP state, a weaker one --- to a double-excited dark state, 
and the weakest one --- to a single-excited UP state. Other signals should clearly be much weaker (for a macroscopic number of molecules). 









