\chapter{Conclusion and future directions}

This chapter briefly summarises the main conclusions of this dissertation and outlines potential extensions of the projects discussed. 



\section{Raman scattering with strongly coupled vibron-polaritons}

As was shown, strong coupling between an infrared photon and a molecular vibron, resulting in the formation of vibron-polaritons, 
naturally leads to redistribution of Raman scattering (RS) signal between the upper and the lower vibron-polariton modes. 
At the same time, the signals are not the same. Indeed, due to mode softening, RS scattering to the lower polariton mode 
increases as coupling strength goes up, while RS to the upper polariton --- reduces. However, the net effect of strong coupling 
on RS is that the total RS signal increases by up to four times in the ultra-strong coupling regime, which contradicts to 
the experiment \cite{enhanced_raman}, while supports and extends the results of independent theoretical calculations presented 
in Ref.~\cite{Raman_Madrid}. Next, due to symmetry, it is clear that there can be no RS to a state with a single dark state. 
However, surprisingly there is finite Raman transition probability to the double-excited dark state (with zero total momentum), 
which has the same order of magnitude as a transition probability to one of single excited polariton modes. 


Given the existence of two independent theoretical papers on vibron-polariton RS with similar results, which contradict to a 
single existing experimental paper \cite{enhanced_raman}, it seems that more experimental measurements would help to clarify 
the story and guide further theoretical modeling. For example, it would help to make sure whether the RS signal enhancement 
is really associated with the vibronic strong coupling or with some other mirror (e.g. plasmonic) effects. Also, measurements 
of the detuning and coupling strength dependence of upper and lower polariton RS amplitudes would be of great use for checking 
theoretical predictions. 











\section{Organic polariton lasing}

The phase diagram of an ordinary (weakly coupled) dye laser was obtained and explained. It was shown that 
increasing the strength of matter-light coupling leads to a smooth transition from a dye laser to a 
strongly-coupled polariton. However, in contrast to common beliefs, at optimised cavity frequency, 
there is no significant lasing threshold reduction as the system enters a strong coupling regime.
On the other hand, in the polariton laser regime, low-threshold lasing can be achieved over a wider 
range of bare cavity photon frequencies compared to a weakly-coupled laser due to a self-tuning effect. 
Also, the method developed in this project allows to answer a lot of other related questions. 

Firstly, to study the emission and absorption spectrum below the lasing threshold, one may go one step 
beyond the mean-field approach using a so-called cumulant expansion \cite{superrad_PRL,kirton2018superradiant}, 
i.e. writing equations for higher-order expectations such as 
$\langle a a \rangle$, $\langle a^{\dagger} a \rangle$, $\langle a \lambda_i \rangle$ and $\langle \lambda_i^m \lambda_j^k \rangle$ 
($m,k$ refer to the index of a molecule). 

Secondly, taking into account many photon modes, one can study mode populations to describe a process 
of thermalisation and condensation, to understand how the physics of weakly coupled photon BEC 
\cite{klaers2010bose, phot_condensation_PRL, phot_condensation_PRA, PhysRevLett.120.040601, radonjic2018interplay} 
changes as one enters the strong coupling regime, or to understand the effect of relatively high temperature on the bottleneck 
effect observed in, e.g. \cite{plumhof2014room}. This may require to write a better master equation to allow transitions 
between different polariton momentum states caused by some low-energy degrees of freedom (which in the current model are 
responsible for dephasing $\Gamma_z$ only), for example adopting the approach developed in Ref.~\cite{del2015quantum}. 

Another direction is to study effects of multiple vibrational modes (for example, to start with, one strongly and one weakly 
coupled to an electronic transition vibrational mode), or to explore the effect of structured baths using Redfield theory
with the true system eigenstates \cite{del2015quantum}. 












\section{Imbalanced polariton condensates}

Within a mean-field approach, there are six ordered states (with a finite region of non-zero expectation 
$\langle h^{}_{Q/2 - k} e_{Q/2 + k} \rangle$) found: a usual balanced polariton condensate with equal 
densities of electrons and holes, imbalanced isotropic and anisotropic condensed state with and without 
uncoupled electrons, and the FFLO state. It is shown that imbalanced condensed states with almost zero 
center of mass exciton momentum are promoted by a photon as its cutoff frequency goes down. 
Moreover, due to small photon mass the resulting polaritonic states are expected to be stable at high 
temperatures akin to usual polariton condensates. These mean-filed results form a firm foundations for 
many other directions. 

The most straightforward extension of this project is to consider other phases, such as, e.g. superconductivity or magnetic 
orders, to understand how they interact with other phases found at the mean-field level.  

Another obvious question is how to formulate a gauge-independent renormalised cutoff-free theory, which though is 
not so obvious how to do. 

An interesting direction would be to explore the effects of topological band structure of real materials, 
to find new phases emerging from topology, Coulomb and a cavity-photon mediated interactions. 
The first step in this direction has recently been made in Ref.~\cite{PhysRevLett.120.157601} considering polariton 
condensation in topological materials. However, the physics of imbalanced polariton condensates and the effect of other 
tunable parameters, such as band inversion and band-coupling strength \cite{PhysRevLett.120.186802}, remains unexplored. 
All these calculations can be done using a variational mean-field approach developed in this thesis updating the 
form of the bare Hamiltonian to include topological band structure. 
        
An apparent but not so straightforward direction is to go beyond mean-field to study the physics of fluctuations. 
Firstly, it is interesting to understand the effects of dressing of polaritons by itinerant electrons, to understand the 
connection between Fermi-polaron-polaritons and trions in regimes of high and low electron doping, to understand how screening 
affects the phase diagrams in different regimes. Another related question is how fluctuations reshape the phase diagram, 
how they affect different ordered states and whether they may induce new ordered states near quantum critical points akin 
to superconductivity induced by critical fluctuations discussed in, e.g. Ref.~\cite{PhysRevB.87.121112, PhysRevLett.114.097001}. 
While it is difficult to include the physics of fluctuations within a variational many-body approach, there are well-established 
field-theoretical methods, which can now be relatively safely used given the existence of a variational mean-field solution. 

While the discussion above implicitly assumed thermal equilibrium calculations, in practice all polaritonic systems are 
essentially non-equilibrium because photons leave a cavity and so an external pumping is always required. Therefore, predictions 
of an equilibrium theory should be considered with a pinch of salt because non-equilibrium effects may wash out some phases or 
bring a system to some particular metastable state, which is not necessarily the global free energy minimum state as has been 
recently shown in Ref.~\cite{sun2019transient}. Therefore, non-equilibrium theory is a natural and necessary extension of this 
project.  








