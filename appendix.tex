\newpage
\appendix

\chapter{Calculation details for the Raman scattering}




\section{$A^2$-term for an harmonical oscillator and a two-level system}
\label{A2_term_append}

In this section I show how a prefactor of an $A^2$-term is derived starting from a QED Hamiltonian in the Coulomb gauge. 

A Hamiltonian of a system of charged particles (CGS) reads 
\begin{equation}
H = \sum_i \frac{1}{2m_i} \left( \vec{p}_i - \frac{q_i}{c} \vec{A}(\vec{r}_i) \right)^2 + 
    \sum_{\vec{k},n} \hbar \omega_{\vec{k}} a^{\dagger}_{\vec{k},n} a^{\mathstrut}_{\vec{k},n} + 
    V_{Coulomb}
\end{equation}
In the Coulomb gauge, the longitudinal part of a vector potential is zero $\vec{A}(t) \equiv \vec{A}_{\perp}(r)$, thus $\vec{p} \vec{A} = \vec{A} \vec{p}$, 
so expanding brackets and supposing that we deal with equivalent particles ($m_i = m$, $q_i = e$) we get 

\begin{equation}
\label{expanded_lm_ham}
H = \sum_i \left[ \frac{\vec{p}_i^2}{2m} - \frac{e}{mc} \vec{A}(\vec{r}_i) \vec{p}_i + \frac{e^2}{2mc^2} \vec{A}^2(\vec{r}_i) \right]
\end{equation}


\subsection{Two-level system}

Making a two-level system approximation, i.e. taking into account only ground $|g\rangle$ and the first excited $|e\rangle$ state 
with energy difference $\varepsilon$, one can get $\langle e | p | g \rangle = \frac{i \varepsilon m }{e\hbar} d_{eg}$ with $d_{eg}$ - dipole matrix element, 
thus within this approximation we get 
\begin{equation}
\frac{e}{mc} p A \rightarrow \frac{i e \varepsilon d}{\hbar e c} \sigma^x_i A = 
    \frac{i e \varepsilon d}{\hbar e c} \sqrt{ \frac{2\pi \hbar c^2}{\omega_c V} } \sigma^x_i (a^{\dagger} e^{-i\vec{k}\vec{r}_i} + ae^{i\vec{k}\vec{r}_i}) 
    \equiv g \sigma^x_i (a^{\dagger} + a)
\end{equation}
for a single lowest-energy photon mode in a 2D cavity (so that $\vec{k} \equiv \vec{k}_{\parallel} = 0$). 
For transition dipole matrix elements there is a sum rule, called the Thomas-Reiche-Kuhn sum rule, which reads 

\begin{equation}
\sum_b |d_{ab} |^2 (E_b - E_a) = \frac{\hbar e^2}{2m}
\end{equation}
which for a two-level systems has a simple form $d_{eg}^2 \varepsilon = \hbar^2 e^2 / 2 m$. Supposing that light wavelength is much larger than 
particle separation, we can approximately set $\vec{A}(\vec{r}_i) \approx \vec{A}$. Therefore, combining all the expressions above, the last part of 
the Hamiltonian \ref{expanded_lm_ham} may be rewritten as follows:
\begin{equation}
\frac{e^2}{2mc^2} \sum_i \vec{A}^2(\vec{r}_i) = \frac{g^2 N}{\varepsilon} (a^{\dagger} + a)^2
\end{equation}
where $N$ is a total number of particles. 



\subsection{Harmonic oscillator}

The matter-light part of the Hamiltonian reads $- \frac{e}{mc} A \sum p_i + \frac{e^2 N}{2mc^2}A^2$, where $A = \sqrt{\frac{2 \pi \hbar c^2}{\omega_c V}}$. 
Momentum operator is $p = i \sqrt{m \hbar \omega_v / 2}(b^{\dagger} - b)$. To get the desired form $(b^{\dagger} + b)$, we can make a unitary transformation 
$b \rightarrow e^{i\pi} b$. Identifying then a prefactor of the term $(b^{\dagger} + b)(a^{\dagger} + a)$ with matter-light coupling $G$, one can readily 
see that the prefactor of the term $(a^{\dagger} + a)^2$ equals to $G^2 N / \omega_v$. 












\section{n-th polariton excitation}
\label{append1}

In this appendix I show how to generalize the expression \ref{MkRWA}, describing the Raman transition amplitude to a single excited polariton mode, to describe the transition to n-th excited bright mode.

For, definiteness, let us do it for UP. As we have shown
\begin{equation}
\mathpzc{M}_{int,0} = \beta_1^{p_1} \beta_2^{p_2}(\beta^*_{n,3})^{p_3} \dots (\beta^*_{n,N+1})^{p_{N+1}},
\end{equation}
so we only need to calculate the second matrix element $\mathpzc{M}_{f_k,int}$ for the transition to $m$-th excited UP:
\begin{equation}
\begin{split}
\mathpzc{M}_{f_k,int} &= \langle 0| (\eta_1+\beta_1)^{p_1}\dots(\eta_{N+1}+\beta_{n,N+1})^{p_{N+1}}e^{-\sum_i\beta^*_{n,i}\eta_i} \frac{\eta_1^{\dagger m}}{\sqrt{m!}} |0\rangle = \\& =
    \sum\limits_l \frac{(-\beta_1)^l}{l!} \langle 0| (\eta_1+\beta_1)^{p_1}\dots(\eta_{N+1}+\beta_{n,N+1})^{p_{N+1}} \eta_1^l |m,0,...,0\rangle.
\end{split}
\end{equation}
Then noticing that $\eta^k | n \rangle = \sqrt{\frac{n!}{(n-k)!}}| n - k \rangle$, we get
\begin{equation}
\begin{split}
\mathpzc{M}_{f_k,int} &=
    \sum\limits_{l=0}^{m} \frac{(-\beta_1)^l \sqrt{m!}}{l!\sqrt{(m-l)!}}
    \langle 0| (\eta_1+\beta_1)^{p_1}\dots(\eta_{N+1}+\beta_{n,N+1})^{p_{N+1}} |m-l,0,...,0\rangle = \\& =
        \beta_2^{p_2}...\beta_{n,N+1}^{p_{N+1}} \sum\limits_{l=0}^{m} \frac{(-\beta_1)^l \sqrt{m!}}{l! \sqrt{(m-l)!}}
         \langle 0| \sum_k C_{p_1}^k \eta_1^{p_1-k} \beta_1^k |m-l \rangle.
\end{split}
\end{equation}
The sum over $k$ has only nonzero term at $k = p_1 - m + l$, so
\begin{equation}
\langle 0| \sum_k C_{p_1}^k \eta_1^{p_1-k} \beta_1^k |m-l \rangle = \sqrt{(m-l)!} \beta_1^{p_1 - m + l} C_{p_1}^{p_1 - m + l},
\end{equation}
thus we clearly get
\begin{equation}
\mathpzc{M}_{f_k,int} = \sqrt{m!}\,
  \mathpzc{M}_{int,0}^* \sum\limits_{l=0}^{m} \frac{(-1)^l}{l!} C_{p_1}^{p_1 - m + l} \beta_1^{2l-m}.
\end{equation}
Although, it is impossible to simplify it further, it is possible to calculate an expression similar to (\ref{likein}) 
(for polaritonic states $\sum_n \rightarrow N$):
\begin{equation}
M = \sqrt{m!} \, N \sum_{\{p_i\}} \prod_i e^{-|\beta_{n,i}|^2}\frac{|\beta_{n,i}|^{2p_i}}{p_i!} \frac{1}{\Delta + \sum_j p_j\omega_j} \sum\limits_{l=0}^{m} \frac{(-1)^l}{l!} C_{p_1}^{p_1 - m + l} \beta_1^{2l-m}.
\end{equation}
Skipping evident from previous derivation steps, now we need to calculate (like in (\ref{likein}))
\begin{equation}
\sqrt{m!} \, \sum_{l=0}^m \frac{(-1)^l}{l!} \beta_1^{2l - m }
\prod\limits_{j \neq 1} e^{|\beta_{nj}|^2 \left( e^{-z \omega_j} - 1 \right)}
        \sum\limits_p \frac{x^p}{p!} C_p^{p-m+l} e^{-\beta_1^2},
\end{equation}
where $x = \beta_1^2 e^{-z \omega_1}.$ First,
\begin{equation}
\sum\limits_p \frac{x^p}{p!} C_{p_1}^{p_1-m+l} = \sum\limits_p \frac{x^p}{p!} \frac{p!}{(p-m+l)!(m-l)!} = ... = \frac{1}{(m-l)!} e^x x^{m-l}.
\end{equation}
Next, the sum over $l$:
\begin{equation}
\label{win}
\begin{split}
&\sqrt{m!} \, \sum_{l=0}^m \frac{(-1)^l}{l!} \beta_1^{2l - m } \frac{x^{m-l}}{(m-l)!} =
  \sqrt{m!} \, \beta_1^{-m} x^m \sum_{l=0}^m \frac{(-x^{-1} \beta_1^2)^l}{l! (m-l)!} = \\& =
    \beta_1^{-m} x^m \frac{1}{\sqrt{m!}} \sum_{l=0}^m \frac{\theta^l m!}{l! (m-l)!} =
      \beta_1^{-m} x^m \frac{1}{\sqrt{m!}} \sum_{l=0}^m \theta^l C_m^l =
        \beta_1^{-m} x^m \frac{1}{\sqrt{m!}} (1 + \theta)^m.
\end{split}
\end{equation}
Now we only need to collect all the terms together. For the sum over $l$ from (\ref{win}) we get:
\begin{equation}
\frac{1}{\sqrt{m!}}\beta_1^{m} \left( e^{-z\omega_1} - 1 \right)^m
\end{equation}
and so on, eventually getting the desired expression (in resonance):
\begin{equation}
M_k^{n} = N \frac{\beta_k^n}{\sqrt{2^nn!}} \int ds e^{-s \Delta}
      \left( e^{-s\omega_k} - 1 \right)^n
      \exp\left[ - \frac 1 2 \sum_i |\beta_i|^2 \left( 1 - e^{-s \omega_i} \right)   \right],
\end{equation}
where $\beta$ was defined in the main text.






\section{N to three modes transformation}
\label{appendA}

In this appendix I show how to transform the description of the model to replace N-mode problem by 3-mode one.

In order to get a three mode description as in (\ref{three}) we can just chose the following basis: $|\psi_a^{(m)} \rangle = (1;0,0,...0)$ for a cavity mode and
$|\psi_b^{(m)} \rangle = (0;0,...,0,1,0,...,0)$ (where nonzero element means vibrationally excited m-th molecule), 
$|\psi_c^{(m)} \rangle = \frac{1}{\sqrt{N-1}}(0;1,...,1,0,1,...,1)$ (zero for m-th molecule)
for molecular modes. So, we can clearly see that if we change now $b \rightarrow b_m$ and $c \rightarrow \frac{1}{\sqrt{N-1}} \sum_{j \neq m} b_j$ in (\ref{three}), 
we reproduce an original Hamiltonian (\ref{hamilt2}). In terms of these basis state, the eigenmodes are: upper and lower polaritons
\begin{equation}
| LP / UP \rangle = |\psi_a \rangle \pm \frac{1}{\sqrt{N}} \left( |\psi_b \rangle + \sqrt{N-1}|\psi_c \rangle \right)
\end{equation}
and dark states
\begin{equation}
| D^{(m)} \rangle = \frac{1}{\sqrt{N}} \left( \sqrt{N-1} |\psi_b \rangle - |\psi_c \rangle \right).
\end{equation}

The form of the dark mode depends on the number of the excited molecule, so if we calculate the transition to the dark state and consequently sum up over all molecules, we should always take a different dark mode, corresponding to the molecule which gets excited. However, it turns out that there is no transition to 
the dark state, so we can neglect it and consider Raman transition only to the bright states. This statement was proven after the Eq.~\ref{rw1} for a different 
set of basis states. Let us prove it for a basis discussed in the current context. 

To prove it, let us calculate the transition probability $P_X$ from some state $|\psi \rangle$ to some reference eigenstate  
$|X \rangle$. So, $P_X = |\sum_m \langle \psi | D_m \rangle \langle D_m | X \rangle |^2 = |\sum_m \theta_m \langle D_m | X \rangle |^2$, 
where the amplitude $\langle \psi | D_m \rangle = \theta_m$ does not depend on the molecule number as was shown in the main text. 
Let us choose a reference eigenstate to be a dark state $| X \rangle = | D_1 \rangle$, which is an eigenmode. 
If so, $\langle D_1 | D_{m \neq 1} \rangle = \frac{1}{N} \left( -2 + \frac{N-2}{N-1} \right) = \frac{-1}{N-1}$. Then summing it over all 
molecules gives $|\sum_m  \langle X | D_m \rangle|^2 = 0$. This means that due to destructive interference we do not have scattering to the dark mode.






\section{Transition amplitude in non RWA and $\delta\omega_v \neq 0$}
\label{appendB}

In this appendix I show how to calculate the transition amplitude beyond RWA and including $\delta\omega_v \neq 0$ effect.

In order to calculate (\ref{summ}), we can define $\mathbf{X} \equiv \mathbf{X}_{\uparrow}$ and rewrite connection relation for two (upper and lower) bases:
\begin{equation}
\mathbf{X}_{\downarrow} = \mathbf{U}_{\downarrow}^{\dagger} \left( \mathbf{U}_{\uparrow}\mathbf{X} - \mathbf{V}_{\uparrow}^{-1} \mathbf{h}_{\uparrow} \right) =
\mathbf{U}_{\downarrow}^{\dagger} \mathbf{U}_{\uparrow} \left( \mathbf{X} - \mathbf{l} \right),
\end{equation}
where we introduced
$
\mathbf{l} = \mathbf{U}_{\uparrow}^{\dagger}\mathbf{V}_{\uparrow}^{-1}\mathbf{h}_{\uparrow} = \Omega_{\uparrow}^{-2}\mathbf{U}_{\uparrow}^{\dagger}\mathbf{h}_{\uparrow}.
$
Then using, for example, the result of harmonic oscillator density matrix calculation, we can compute the sum (which is a weighted sum of the product of Hermite polynomials)
\begin{equation}
\begin{split}
\sum_m \psi_m \left( \sqrt{\omega } X \right)
        \psi_m \left( \sqrt{\omega } X' \right)  e^{-s m \omega} =
        \\ =
        \frac{1}{\sqrt{\pi}} \frac{1}{\sqrt{1 - e^{-2 s \omega}}}
        \exp\left[ - \frac{\omega}{2} \left(
        \frac{X^2 + X'^2}{\tanh(s \omega)} - \frac{ 2 X X'}{\sinh(s \omega)}
        \right)
        \right],
\end{split}
\end{equation}
so with this expression for the sum over intermediate states and definition (\ref{summ}) we get
\begin{equation}
\begin{split}
M_k &= \frac{\sqrt{2 \omega_{k, \downarrow}} }{\pi^3}
      \left[ \mathbf{U}_{\downarrow}^{\dagger}\mathbf{U}_{\uparrow} \right]_{kr}
      \int ds e^{-s \Delta} \int \prod_i \left( d^3 x_i d^3 x'_i \sqrt{\omega_{i,\uparrow} \omega_{i,\downarrow}} \right)  (\mathbf{x}_r - \mathbf{l}_r) \\&
      \exp
      \Bigg[
          - \frac 1 2
          \Big(
          (\mathbf{x}-\mathbf{l})^\intercal \mathbf{U}_{\uparrow}^\intercal \mathbf{U}^{\mathstrut}_{\downarrow} \mathbf{\Omega}^{\mathstrut}_{\downarrow} \mathbf{U}_{\downarrow}^\intercal \mathbf{U}^{\mathstrut}_{\uparrow} (\mathbf{x} - \mathbf{l}) +
          (\mathbf{x}'-\mathbf{l})^\intercal \dots (\mathbf{x}' - \mathbf{l})
          \Big)
      \Bigg] \\&
              \prod_i \frac{1}{\sqrt{1 - e^{-2 s \omega_{i,\uparrow}}}}
        \exp\left[ - \frac{\omega_{i,\uparrow}}{2} \left(
        \frac{X_i^2 + X_i'^2}{\tanh(s \, \omega_{i,\uparrow})} - \frac{ 2 X_i X_i'}{\sinh(s \, \omega_{i,\uparrow})}
        \right)
        \right] \equiv \\& \equiv
            \frac{\sqrt{2 \omega_{k, \downarrow}} }{\pi^3}
            \left[ \mathbf{U}_{\downarrow}^{\dagger}\mathbf{U}_{\uparrow} \right]_{kr}
            \int ds e^{-s \Delta} \prod_i
            \left(
            \sqrt{\frac{\omega_{i,\uparrow}\omega_{i,\downarrow}}{1 - e^{-2s \omega_{i,\uparrow}}}}
            \right) O_r(s),
\end{split}
\end{equation}
where, defining
$
\mathbf{R} \equiv \mathbf{U}_{\uparrow}^\intercal \mathbf{U}^{\mathstrut}_{\downarrow} \mathbf{\Omega}^{\mathstrut}_{\downarrow} \mathbf{U}_{\downarrow}^\intercal \mathbf{U}^{\mathstrut}_{\uparrow}
$,
$
P_i \equiv \frac{\omega_{i,\uparrow}}{\tanh(s\omega_{i,\uparrow})}
$ and
$
Q_i \equiv \frac{\omega_{i,\uparrow}}{\sinh(s\omega_{i,\uparrow})},
$
for the vector $O_r(s)$ we have
\begin{equation}
\begin{split}
O_r(s) = \int d^3x d^3x'
          \exp
          \Bigg[
          - \frac 1 2 (\mathbf{x}-\mathbf{l})^\intercal \mathbf{R} (\mathbf{x} - \mathbf{l}) + \\ +
          (\mathbf{x}'-\mathbf{l})^\intercal \mathbf{R} (\mathbf{x}' - \mathbf{l})
          - \frac 1 2 \sum_i \Big( P_i(X_i^2 + X_i'^2) - \\ - 2 Q_i X_i X_i' \Big)
          \Bigg]
          (X_r - l_r).
\end{split}
\end{equation}
To proceed further, we can notice that this is a 6 dimensional Gaussian integral, so it can be calculated analytically. Defining then $\mathbf{z}^\intercal \equiv (\mathbf{x}^\intercal,\mathbf{x}'^\intercal)$ for convenience, we get
\begin{equation}
O_r(s) = \int d^6 z (z_r - l_r) \exp\left( -\frac 1 2 \mathbf{z}^\intercal \mathbf{A} \mathbf{z} + \mathbf{q}^\intercal \mathbf{z} - c \right),
\end{equation}
where
\sbox0{$\begin{matrix}1&2&3\\0&1&1\\0&0&1\end{matrix}$}
\begin{equation}
\mathbf{A} =
      \left(
      \begin{array}{c|c}
      \vphantom{\usebox{0}}\makebox[\wd0]{\large $\mathbf{P + R}$ }&\makebox[\wd0]{\large $- \mathbf{Q}$}\\
      \hline
        \vphantom{\usebox{0}}\makebox[\wd0]{\large $- \mathbf{Q} $}&\makebox[\wd0]{\large $\mathbf{P + R}$}
      \end{array}
      \right),
\end{equation}
where $\mathbf{P,Q}$ - diagonal matrixes (defined in the main text), $\mathbf{q}^\intercal = (\mathbf{l}^\intercal \mathbf{R}, \mathbf{l}^\intercal \mathbf{R})$, $c = \mathbf{l}^\intercal \mathbf{R} \mathbf{l}$. Thus, computing Gaussian integral, we eventually obtain
\begin{equation}
\mathbf{O}_r(s) = \left( \mathbf{A}^{-1}\mathbf{q} - \mathbf{l} \right)_r \frac{(2\pi)^3}{\sqrt{\det(\mathbf{A})}} \exp\left( \frac 1 2 \mathbf{q}^\intercal \mathbf{A}^{-1}\mathbf{q} - \mathbf{l}^\intercal \mathbf{R} \mathbf{l} \right),
\end{equation}
and eventually we obtain the final expression (\ref{final}).







\chapter{Calculation details for the Organic polariton lasing}

\section{Molecular transition weights calculation}
\label{molecular_transition_from_dens_matr}

In this appendix I show how to obtain the compositions of molecular transitions involved in lasing, which were presented in 
Fig.~\ref{weak_coupling_summary}(d), Fig.~\ref{spectrum_and_DM_cuts__01}(d-f), Fig.~\ref{spectrum_and_DM_cuts__10}(d-f). 

To determine the weights of molecular transitions corresponding to a given unstable mode, 
we need to construct a molecular density matrix, which can be built from the eigenvector 
$\upsilon = \left( \delta \alpha , \delta \alpha^* , \mathbf{\delta\ell} \right)^{\intercal}$ of the stability
matrix $\mathcal{M}$. We can then extract the matter part $\delta \ell_i$ and so construct the corresponding molecular
density matrix,
\begin{math}
  \rho(t) = \rho_{\text{ns}} + 
  \mathcal{A} (\delta \ell_i e^{\xi t} + \delta \ell_i^{\ast} e^{\xi^{\ast} t}) 
  \lambda_i/2
\end{math}
where $\mathcal{A}$ is an arbitrary amplitude and $\rho_{\text{ns}}$ is the normal state density matrix, which form is 
not important now. (For simplicity of notation, we neglect the superscripts on $\delta \ell$ and $\xi$ labeling eigenmodes.)
The complex conjugates are required in order to guarantee Hermiticity (because the Gell Mann matrices 
are chosen to be Hermitian). This is crucial since the equations of motion mix $\ell_i$ and $\ell_i^\ast$.
To find the amplitude of the oscillatory component of a given element of the density matrix $\rho_{ij}$, we first 
define the matrix $r = \delta \ell_i \lambda_i/2$ and then note the oscillatory component of $\rho_{ij}$ takes the 
form $\delta \rho_{ij} = r_{ij} e^{\xi t} + r_{ji}^\ast e^{\xi^\ast t}$ (because Gell Mann matrices $\lambda_i$ are Hermitian).  

The diagonal components of a density matrix give the molecular state populations. Defining $Re(\xi) = \xi'$ and $Im(\xi) = \xi''$, 
for the diagonal components of the density matrix we can get 
$\delta \rho_{ii} = 2|r_{ii}| \cos\big( \xi^{\prime\prime} t + \text{Arg} (r_{ii})\big) e^{\xi^\prime t}$. 
The nondiagonal ones correspond to coherences. In particular, the block of the density matrix which is off-diagonal in terms 
of electronic states gives the weights of molecular transitions involved in lasing.  


For the off diagonal components, in general $|r_{ij}| \neq |r_{ji}|$; we thus expect that the quantity $\delta \rho_{ij}$ 
traces an elliptical spiral in the Argand plane, 
$\delta \rho_{ij} = [r_A \cos(  \xi^{\prime\prime} t + \phi) + i r_B \sin(  \xi^{\prime\prime} t + \phi)] e^{i \theta+\xi^\prime t} $.  
The semi-major and semi-minor axes of this ellipse are given by $r_{A,B} = |r_{ij}| \pm |r_{ji}|$, 
while $\phi,\theta=[\text{Arg}(r_{ij}) \pm \text{Arg}(r_{ji})]/2$. Given this behavior, we define the amplitude of a given component 
by the semi-major axis.  The contribution (weight) of a given molecular transition $(n-m)$ to the lasing mode thus corresponds to the amplitude
$|r_{n\downarrow, m\uparrow}| + |r_{m\uparrow,n\downarrow}|$.










\chapter{Imbalanced polariton condensates}

\section{Numerical implementation}
\label{calc_details_fflo}

In this appendix I discuss details of numerical solution of the model discussed in the main text. 

To obtain the solution, one need to optimize the functional (\ref{total_free_energy}) over $4N + 1$ ($N$ is the number of momentum grid points, 
which is, e.g. $10^4$ for a 100 by 100 momentum-space grid) variables 
$l_{\vec{k}} \equiv \{ \eta^e_{\vec{k}}, \eta^h_{\vec{k}}, \Delta_{\vec{k}}, \phi, \vec{Q} \}$. 
First, to simplify a solution, 
we may choose a direction of spontaneous symmetry breaking $\vec{Q} = (Q_x, 0)$ and so to remove $N-1$ variables. Secondly, given that 
we expect to have asymmetry in the direction of $\vec{Q} = (Q_x, 0)$, we may restrict our ansatz to have variables symmetric with respect 
to reflection $k_y \rightarrow - k_y$, which leaves us around $1.5N$ rather than original $4N$ variables. 


Nevertheless, even with this reduction of the number of variables, we are still left with of order of $10^3 - 10^4$ variables for a physically 
reasonable momentum-grid size. Unfortunately, starting from arbitrary initial conditions for $l_{\vec{k}}$, 
minimization routine often ends up in some local minimum, which may be far away from the global minimum, so it fails to find a system lowest free energy state. 
This requires to provide good initial conditions for minimization. However, even before this, one should provide a gradient of (\ref{total_free_energy})
for minimization. While the derivatives over $\{ \eta^e_k, \eta^h_k, \Delta_k, \phi \}$ can be calculated analytically, there is no closed 
form of $\delta \mathcal{F} / \delta Q$. This is why the variable $Q$ is treated separately from the others. For this reason, to minimize 
with respect to $Q$, free energy functional is optimized at some fixed $Q$, then this calculation is repeated for many values of $Q$ and 
eventually one can find which $Q$ minimizes the free energy. Therefore, hereafter some fixed $Q$ will be assumed. 


Let us discuss how to provide good initial conditions for minimization. First, the simplest thing to do is to build a system normal state 
(i.e. $\Delta_k \equiv 0$ and $\phi \equiv 0$) solution. Without the electrostatic term ($\alpha = 0$), normal state equations (\ref{eta_e_h}) 
may well be solved iteratively, which can be symbolically written as $x^{new} = x^{old} + F[x^{old}]$.
However, with arbitrary $\alpha$, the iterative procedure turns out to be unstable. 
This problem may be fixed by introducing some small admixing parameter $\zeta \ll 1$ ($x^{new} = x^{old} + \zeta F[x^{old}]$). 
Although this trick stabilises the solution, it does not guarantee that the resulting state is the lowest energy state. 
For these reasons a different successive minimization approach was used. The problem is that minimization does not work well 
at low temperature, where there is a sharp Fermi surface. To tackle the problem of arbitrary $\alpha$ and at low temperature, 
the following algorithm is used:
\begin{enumerate}
 \item Find normal state at $\alpha = 0$ and at high temperature, where a Fermi surface is smooth
 \item Use the previous solution as an initial condition for minimization at higher $\alpha$
 \item Repeat the previous step until reaching the target value of $\alpha$
 \item After this slightly reduce temperature and then repeat it going to lower temperature using a previous step solution 
        as an initial condition for the next step
\end{enumerate}
This allows to overcome the problem of rather large $\alpha$ and low temperature $T$ and so to find a normal state solution ($\Delta_{\vec{k}} = 0$ and 
$\phi = 0$). Next, the solution for $\eta^{e/h}_{\vec{k}}$ is used as an initial condition for a full ansatz optimization, i.e. allowing 
finite gap function $\Delta_{\vec{k}}$ and photon density $\phi$. To obtain a good initial condition for $\Delta_{\vec{k}}$, a gap equation 
(\ref{gap_equation}) is solved iteratively using normal state solutions for $\eta^{e/h}_{\vec{k}}$.
A solution obtained in this way is then used as an initial condition for optimization. 

















